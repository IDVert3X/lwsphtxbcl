/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ObjectDumper
{
    public static String dump(Object o)
    {
        StringBuilder builder = new StringBuilder();

        builder.append(o.getClass().getSimpleName())
                .append(" : ")
                .append("{\n");

        List<Field> fields = getAllFields(o);
        int lfn = getLongestFieldLength(fields);

        for (Field field : fields)
            builder.append(formatField(field, lfn, o));

        builder.append("}");
        return builder.toString();
    }

    public static void print(Object o)
    {
        System.out.println(dump(o));
    }

    private static List<Field> getAllFields(Object o)
    {
        Class c = o.getClass();
        List<Field> fields = new ArrayList<>();

        // recursively get all fields
        while (c != null) {
            for (Field f : c.getDeclaredFields()) {
                // ignore constants
                if (Modifier.isStatic(f.getModifiers()) && Modifier.isFinal(f.getModifiers()))
                    continue;
                // make sure the field is accessible
                if (!Modifier.isPublic(f.getModifiers()))
                    f.setAccessible(true);
                fields.add(f);
            }
            c = c.getSuperclass();
        }

        return fields;
    }

    private static int getLongestFieldLength(List<Field> fields)
    {
        int longestFieldLength = 1;
        for (Field f : fields) {
            int len = f.getName().length();
            if (len > longestFieldLength)
                longestFieldLength = len;
        }
        return longestFieldLength;
    }

    private static String formatField(Field field, int fieldNameLength, Object o)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("    ").append(field.getName());

        for (int i = field.getName().length(); i < fieldNameLength; i++)
            builder.append(" ");

        builder.append(" : ");

        String s;
        try {
            Object val = field.get(o);
            s = val == null ? "<null>" : objToString(val);
        } catch (IllegalAccessException e) {
            s = "<inaccessible field>";
        }
        if (s.contains("\n")) {
            builder.append("\n");
            for (String line : s.split("\n")) {
                builder.append("        ");
                builder.append(line).append("\n");
            }
        } else {
            builder.append(s);
        }

        builder.append("\n");

        return builder.toString();
    }

    private static String objToString(Object val)
    {
        return hasOwnToStringImplementation(val.getClass())
                ? val.toString()
                : ObjectDumper.dump(val);
    }

    private static boolean hasOwnToStringImplementation(Class<?> clazz)
    {
        Method m;
        try {
            m = clazz.getMethod("toString");
        } catch (NoSuchMethodException e) {
            // Will never happen, every class ( Object ) has a toString method
            return false;
        }
        return (m.getDeclaringClass() != Object.class);
    }
}
