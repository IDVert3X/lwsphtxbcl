/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

public class ValidationUtils
{
    /**
     * Verify that given precondition holds true.
     *
     * @param assertionResult Assertion value
     * @param errorMessage    Error message to throw if the assertion fails
     * @param exceptionType   Type of exception to throw if the assertion fails
     */
    public static <T extends RuntimeException> void verifyPrecondition(
            boolean assertionResult,
            String errorMessage,
            Class<T> exceptionType
    )
    throws T
    {
        if (!assertionResult) {
            try {
                throw exceptionType.getConstructor(String.class).newInstance(errorMessage);
            } catch (NoSuchMethodException | IllegalAccessException |
                    InstantiationException | InvocationTargetException e) {
                System.err.println("Unable to throw \"" + exceptionType.getSimpleName() +
                        "\", " + "falling back to RuntimeException.");
                throw new RuntimeException(errorMessage);
            }
        }
    }

    /**
     * Verify that given precondition holds true.
     *
     * @param assertionResult Assertion value
     * @param errorMessage    Error message to throw if the assertion fails
     */
    public static void verifyPrecondition(boolean assertionResult, String errorMessage)
    {
        verifyPrecondition(assertionResult, errorMessage, IllegalArgumentException.class);
    }

    /**
     * Verify that the given objects are not null
     *
     * @param objects Objects that should not be null
     */
    public static void verifyNotNull(Object... objects)
    throws NullPointerException
    {
        for (Object object : objects)
            Objects.requireNonNull(object, "Parameter may not be null");
    }
}
