/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils;

public class ByteArrayBuilder
{
    private byte[] binary;

    public ByteArrayBuilder()
    {
        this.binary = new byte[]{};
    }

    public ByteArrayBuilder(byte[]... bytes)
    {
        this.binary = BinUtils.concat(bytes);
    }

    public ByteArrayBuilder append(byte[] bytes)
    {
        this.binary = BinUtils.concat(binary, bytes);
        return this;
    }

    public ByteArrayBuilder append(byte b)
    {
        this.binary = BinUtils.add(binary, b);
        return this;
    }

    public byte[] getBytes()
    {
        return binary;
    }
}
