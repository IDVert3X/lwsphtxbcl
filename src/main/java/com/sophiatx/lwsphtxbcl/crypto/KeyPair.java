/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.crypto;

import com.sophiatx.lwsphtxbcl.chain.model.PrivateKey;
import com.sophiatx.lwsphtxbcl.chain.model.PublicKey;
import com.sophiatx.lwsphtxbcl.encoding.KeyEncoder;
import com.sophiatx.lwsphtxbcl.encoding.exception.KeyFormatException;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPublicKey;

import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Objects;

public class KeyPair
{
    private final PrivateKey privateKey;
    private final PublicKey publicKey;

    /**
     * Instantiate a new key pair object from the given key pair
     *
     * @param privateKey Private key
     * @param publicKey  Public key
     */
    public KeyPair(BigInteger privateKey, BigInteger publicKey)
    {
        this.privateKey = new PrivateKey(privateKey);
        this.publicKey = new PublicKey(publicKey);
    }

    /**
     * Instantiate a new key pair object from the given key pair
     *
     * @param privateKey Private key
     * @param publicKey  Public key
     */
    public KeyPair(PrivateKey privateKey, PublicKey publicKey)
    {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    /**
     * Instantiate a new key pair object from the given key pair
     *
     * @param wif    Private key in WIF format
     * @param pubKey Public key in its textual representation
     * @throws KeyFormatException If the key format/checksum is invalid
     */
    public KeyPair(String wif, String pubKey)
    {
        this.privateKey = new PrivateKey(KeyEncoder.decodeWif(wif));
        this.publicKey = new PublicKey(KeyEncoder.decodePublicKey(pubKey));
    }

    /**
     * Instantiate a new key pair object from the given private key
     *
     * @param privateKey Private key
     */
    public KeyPair(BigInteger privateKey)
    {
        this(privateKey, Keys.publicKeyFromPrivate(privateKey));
    }

    /**
     * Instantiate a new key pair object from the given private key
     *
     * @param privateKey Private key
     */
    public KeyPair(PrivateKey privateKey)
    {
        this(privateKey, privateKey.derivePublicKey());
    }

    /**
     * Instantiate a new key pair object from the given WIF key
     *
     * @param wifKey Private key in WIF format
     * @throws KeyFormatException If the key format/checksum is invalid
     */
    public KeyPair(String wifKey)
    throws KeyFormatException
    {
        this(KeyEncoder.decodeWif(wifKey));
    }

    /**
     * Securely generate a new key pair
     *
     * @return Generated key pair
     */
    public static KeyPair generate()
    {
        java.security.KeyPair keyPair;
        try {
            keyPair = Keys.createSecp256k1KeyPair();
        } catch (NoSuchProviderException | NoSuchAlgorithmException | InvalidAlgorithmParameterException e) {
            throw new RuntimeException("Could not generate Secp256k1 key pair. This should never happen.");
        }

        BCECPrivateKey privateKey = (BCECPrivateKey) keyPair.getPrivate();
        BCECPublicKey publicKey = (BCECPublicKey) keyPair.getPublic();

        BigInteger privateKeyValue = privateKey.getD();

        // we use compressed public keys
        byte[] publicKeyBytes = publicKey.getQ().getEncoded(true);
        BigInteger publicKeyValue = new BigInteger(publicKeyBytes);

        return new KeyPair(privateKeyValue, publicKeyValue);
    }

    public PrivateKey getPrivateKey()
    {
        return privateKey;
    }

    public PublicKey getPublicKey()
    {
        return publicKey;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof KeyPair)) return false;
        KeyPair keyPair = (KeyPair) o;
        return Objects.equals(privateKey, keyPair.privateKey) &&
                Objects.equals(publicKey, keyPair.publicKey);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(privateKey, publicKey);
    }
}
