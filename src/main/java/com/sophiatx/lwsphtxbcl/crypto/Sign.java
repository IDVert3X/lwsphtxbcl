/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.crypto;

import com.sophiatx.lwsphtxbcl.utils.NumUtils;
import org.bouncycastle.asn1.x9.X9IntegerConverter;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.crypto.signers.HMacDSAKCalculator;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.custom.sec.SecP256K1Curve;

import java.math.BigInteger;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

public class Sign
{
    /**
     * Signs the given hash with the private key of the given key pair.
     *
     * @param digest the hash to sign
     * @return A {@link CompactSignature} of the hash
     */
    public static CompactSignature signDigest(byte[] digest, KeyPair keyPair)
    {
        ECPrivateKeyParameters privKey = new ECPrivateKeyParameters(
                keyPair.getPrivateKey().getKey(), Keys.PARAMS
        );

        ECDSASigner signer = new ECDSASigner(new HMacDSAKCalculator(new SHA256Digest()));
        signer.init(true, privKey);
        BigInteger[] components = signer.generateSignature(digest);
        BigInteger r = components[0];
        BigInteger s = components[1];
        int recId = Sign.findRecId(r, s, digest, keyPair.getPublicKey().getKey());

        return new CompactSignature(recId, r, s);
    }

    /**
     * Work backwards to figure out the recId needed to recover the signature.
     *
     * @param r           R component of the signature
     * @param s           S component of the signature
     * @param messageHash Hash of the data that was signed.
     * @param publicKey   Public key we expect to be a signer of the message.
     * @return recId used to recover the signature
     */
    private static Integer findRecId(
            BigInteger r,
            BigInteger s,
            byte[] messageHash,
            BigInteger publicKey
    )
    {
        int recId = -1;

        for (int i = 0; i < 4; i++) {
            BigInteger k = recoverPublicKey(new CompactSignature(i, r, s), messageHash);
            if (k != null && k.equals(publicKey)) {
                recId = i;
                break;
            }
        }

        if (recId == -1)
            throw new RuntimeException("Could not construct a recoverable key. This should never happen.");

        return recId;
    }

    /**
     * <p>Given the components of a signature and a selector value, recover and
     * return the public key that generated the signature according to the
     * algorithm in SEC1v2 section 4.1.6.</p>
     *
     * <p>The recId is an index from 0 to 3 which indicates which of the 4
     * possible keys is the correct one. Because the key recovery operation
     * yields multiple potential keys, the correct key must either be stored
     * alongside the signature, or you must be willing to try each recId in turn
     * until you find one that outputs the key you are expecting.</p>
     *
     * <p>If this method returns null it means recovery was not possible and
     * recId should be iterated.</p>
     *
     * <p>Given the above two points, a correct usage of this method is inside
     * a for loop from 0 to 3, and if the output is null OR a key that is not
     * the one you expect, you try again with the next recId.</p>
     *
     * @param sig    Signature
     * @param digest Message ( TX digest ) that was signed
     * @return An ECKey containing only the public part, or null if recovery wasn't possible.
     */
    public static BigInteger recoverPublicKey(
            CompactSignature sig,
            byte[] digest
    )
    {
        verifyPrecondition(digest != null, "digest cannot be null");
        int recId = sig.getRecId();

        BigInteger n = Keys.SPEC.getN();
        BigInteger i = BigInteger.valueOf((long) recId / 2);
        BigInteger x = sig.getR().add(i.multiply(n));
        BigInteger prime = SecP256K1Curve.q;

        if (x.compareTo(prime) >= 0) {
            // Cannot have point co-ordinates larger than this as everything
            // takes place modulo Q.
            return null;
        }

        // Compressed keys require you to know an extra bit of data about the
        // y-coord as there are two possibilities. So it's encoded in the recId.
        X9IntegerConverter x9 = new X9IntegerConverter();
        byte[] compEnc = x9.integerToBytes(x, 1 + x9.getByteLength(Keys.CURVE));
        compEnc[0] = (byte) ((recId & 1) == 1 ? 0x03 : 0x02);
        ECPoint R = Keys.CURVE.decodePoint(compEnc);

        if (!R.multiply(n).isInfinity()) {
            // If nR != point at infinity
            return null;
        }

        // noinspection ConstantConditions - checked in verifyPrecondition
        BigInteger e = new BigInteger(1, digest);
        BigInteger eInv = BigInteger.ZERO.subtract(e).mod(n);
        BigInteger rInv = sig.getR().modInverse(n);
        BigInteger srInv = rInv.multiply(sig.getS()).mod(n);
        BigInteger eInvrInv = rInv.multiply(eInv).mod(n);
        ECPoint q = ECAlgorithms.sumOfTwoMultiplies(Keys.SPEC.getG(), eInvrInv, R, srInv);

        return NumUtils.toBigInt(q.getEncoded(true));
    }
}
