/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.sophiatx.lwsphtxbcl.chain.Configuration;
import com.sophiatx.lwsphtxbcl.comm.exception.CommunicationException;
import com.sophiatx.lwsphtxbcl.comm.exception.ConnectionException;
import com.sophiatx.lwsphtxbcl.comm.exception.InvalidResponseException;
import com.sophiatx.lwsphtxbcl.comm.exception.ResponseErrorException;
import com.sophiatx.lwsphtxbcl.comm.exception.ResponseMappingException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.stream.Collectors;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

public class Connector
{
    private static final ObjectMapper mapper;
    private final URL endpoint;
    private final int connectTimeout;
    private final int readTimeout;

    static {
        mapper = new ObjectMapper();

        TimeZone tZone = TimeZone.getTimeZone(Configuration.TIME_ZONE);
        SimpleDateFormat df = new SimpleDateFormat(Configuration.DATE_FORMAT);
        df.setTimeZone(tZone);

        mapper.setDateFormat(df);
        mapper.setTimeZone(tZone);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    /**
     * Initialize a new connector instance with default timeouts (5s, 30s)
     *
     * @param host Blockchain daemon host
     * @param port Blockchain daemon port
     */
    public Connector(Inet4Address host, int port)
    {
        this(host, port, 5000, 30000);
    }

    /**
     * Initialize a new connector instance
     *
     * @param host           Blockchain daemon host
     * @param port           Blockchain daemon port
     * @param connectTimeout Connect timeout ( in milliseconds )
     * @param readTimeout    Read timeout ( in milliseconds )
     */
    public Connector(Inet4Address host, int port, int connectTimeout, int readTimeout)
    {
        verifyNotNull(host);
        verifyPrecondition(port > -1 && port < 65535, "Port out of range");
        verifyPrecondition(connectTimeout > 0, "Connection timeout must be at least 1ms");
        verifyPrecondition(readTimeout > 0, "Read timeout must be at least 1ms");

        try {
            String address = "http://" + host.getHostAddress() + ":" + port;
            this.endpoint = new URL(address);
        } catch (MalformedURLException e) {
            throw new RuntimeException(
                    "Unable to construct URL from given network parameters", e);
        }

        this.connectTimeout = connectTimeout;
        this.readTimeout = readTimeout;
    }

    /**
     * Execute the given {@link AbstractMappableRequest}, reading & validating
     * the response from the node and mapping to the corresponding model.
     *
     * @param req          Request to execute
     * @param <TargetType> Type the response object will be mapped to
     * @return Mapped object
     * @throws ResponseErrorException   When the response contains errors
     * @throws InvalidResponseException When the received response is invalid
     * @throws ResponseMappingException When the response object can not be
     *                                  mapped to the given model
     * @throws CommunicationException   Anything else that could went wrong
     */
    public <TargetType> TargetType execute(AbstractMappableRequest<TargetType> req)
    throws ResponseErrorException, ResponseMappingException, InvalidResponseException,
           CommunicationException
    {
        JsonNode result = execute((AbstractRequest) req);

        try {
            return mapper.convertValue(result, req.getTargetType(mapper.getTypeFactory()));
        } catch (IllegalArgumentException e) {
            throw new ResponseMappingException(e);
        }
    }

    /**
     * Execute the given {@link AbstractRequest}, reading & validating the
     * response from the blockchain daemon and returning a {@link Response}.
     *
     * @param req Request to execute
     * @return Response object
     * @throws ResponseErrorException   When the response contains errors
     * @throws InvalidResponseException When the received response is invalid
     * @throws CommunicationException   Network failure / other problems
     */
    public JsonNode execute(AbstractRequest req)
    throws ResponseErrorException, InvalidResponseException, CommunicationException
    {
        String responseBody = sendRequest(req);
        JsonNode responseTree;

        try {
            responseTree = mapper.readTree(responseBody);
        } catch (IOException e) {
            throw new InvalidResponseException("Response is not a valid JSON");
        }

        if (!responseTree.isObject())
            throw new InvalidResponseException("Response is not an object");

        Response res = new Response(responseTree); // InvalidResponseException
        if (!res.hasExpectedId(req.getId()))
            throw new InvalidResponseException("Invalid response ID");
        if (res.isFailed())
            throw new ResponseErrorException(res.getErrorMessage(), res.getError());

        return res.getResult();
    }

    private String sendRequest(AbstractRequest req)
    throws ConnectionException
    {
        URLConnection con = createConnectionObject();

        try {
            con.connect();
        } catch (IOException e) {
            throw new ConnectionException("Unable to connect", e);
        }

        writeRequestBody(con, req);
        return readResponse(con);
    }

    private URLConnection createConnectionObject()
    {
        try {
            URLConnection connection = endpoint.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(connectTimeout);
            connection.setReadTimeout(readTimeout);
            return connection;
        } catch (IOException e) {
            // should never happen
            throw new RuntimeException("Unable to create URLConnection", e);
        }
    }

    private static void writeRequestBody(URLConnection con, AbstractRequest req)
    throws ConnectionException
    {
        try (OutputStream stream = con.getOutputStream()) {
            stream.write(serializeRequest(req));
        } catch (IOException e) {
            throw new ConnectionException("Unable to write request body", e);
        }
    }

    private static String readResponse(URLConnection con)
    throws ConnectionException
    {
        try (InputStreamReader isr = new InputStreamReader(
                con.getInputStream(),
                StandardCharsets.UTF_8
        )) {
            return new BufferedReader(isr).lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new ConnectionException("Unable to read response", e);
        }
    }

    private static byte[] serializeRequest(AbstractRequest req)
    throws IllegalArgumentException
    {
        try {
            return mapper.writeValueAsString(req).getBytes(StandardCharsets.UTF_8);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Invalid request object", e);
        }
    }
}
