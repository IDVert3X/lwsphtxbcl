/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.condenser.object;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;

import java.util.Objects;

/**
 * This object should only be instantiated by Jackson using reflection
 */
public class LegacyChainProperties
{
    @JsonProperty("account_creation_fee")
    private Asset accountCreationFee;

    @JsonProperty("maximum_block_size")
    private UInteger maximumBlockSize;

    public Asset getAccountCreationFee()
    {
        return accountCreationFee;
    }

    public UInteger getMaximumBlockSize()
    {
        return maximumBlockSize;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof LegacyChainProperties)) return false;
        LegacyChainProperties that = (LegacyChainProperties) o;
        return Objects.equals(accountCreationFee, that.accountCreationFee) &&
                Objects.equals(maximumBlockSize, that.maximumBlockSize);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(accountCreationFee, maximumBlockSize);
    }
}
