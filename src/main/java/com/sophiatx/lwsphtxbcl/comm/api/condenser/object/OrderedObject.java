/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.condenser.object;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonFormat(shape = JsonFormat.Shape.ARRAY)
@JsonPropertyOrder({"order", "object"})
public class OrderedObject<T>
{
    private final Integer order;
    private final T object;

    @JsonCreator
    public OrderedObject(
            @JsonProperty("order") Integer order,
            @JsonProperty("object") T object
    )
    {
        verifyNotNull(order, object);
        this.order = order;
        this.object = object;
    }

    public Integer getOrder()
    {
        return order;
    }

    public T getObject()
    {
        return object;
    }
}
