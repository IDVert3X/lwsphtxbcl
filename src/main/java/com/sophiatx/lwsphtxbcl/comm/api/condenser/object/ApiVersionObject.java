/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.condenser.object;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * This object should only be instantiated by Jackson using reflection
 */
public class ApiVersionObject
{
    @JsonProperty("blockchain_version")
    private String blockchainVersion;

    @JsonProperty("sophiatx_revision")
    private String sophiatxRevision;

    @JsonProperty("fc_revision")
    private String fcRevision;

    @JsonProperty("chain_id")
    private String chainId;

    public String getBlockchainVersion()
    {
        return blockchainVersion;
    }

    public String getSophiatxRevision()
    {
        return sophiatxRevision;
    }

    public String getFcRevision()
    {
        return fcRevision;
    }

    public String getChainId()
    {
        return chainId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ApiVersionObject)) return false;
        ApiVersionObject that = (ApiVersionObject) o;
        return Objects.equals(blockchainVersion, that.blockchainVersion) &&
                Objects.equals(sophiatxRevision, that.sophiatxRevision) &&
                Objects.equals(fcRevision, that.fcRevision) &&
                Objects.equals(chainId, that.chainId);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(blockchainVersion, sophiatxRevision, fcRevision, chainId);
    }
}
