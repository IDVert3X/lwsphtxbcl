/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.condenser.request;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.sophiatx.lwsphtxbcl.comm.AbstractMappableRequest;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.ApiAccountObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class GetAccountsRequest
        extends AbstractMappableRequest<List<ApiAccountObject>>
{
    private List<String> accountNames = new ArrayList<>();

    public GetAccountsRequest(List<String> accountNames)
    {
        Objects.requireNonNull(accountNames, "List of account names to fetch cannot be null");
        accountNames.parallelStream().forEach(this::addAccount);
    }

    public GetAccountsRequest(String accountName)
    {
        addAccount(accountName);
    }

    /**
     * Add a account name to the list of accounts to fetch
     *
     * @param accountName A name of the account to fetch
     * @return GetAccountsRequest
     */
    public GetAccountsRequest addAccount(String accountName)
    throws IllegalStateException
    {
        Objects.requireNonNull(accountName, "Name of the account to fetch cannot be null");
        accountNames.add(accountName);
        return this;
    }

    @Override
    public String getMethodName()
    {
        return "get_accounts";
    }

    @Override
    public Object getParams()
    {
        return Collections.singletonList(accountNames);
    }

    @Override
    public JavaType getTargetType(TypeFactory tf)
    {
        return tf.constructCollectionType(List.class, ApiAccountObject.class);
    }
}
