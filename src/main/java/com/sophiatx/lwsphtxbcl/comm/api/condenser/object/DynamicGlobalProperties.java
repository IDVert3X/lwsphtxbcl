/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.condenser.object;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.chain.model.BlockId;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UByte;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * This object should only be instantiated by Jackson using reflection
 */
public class DynamicGlobalProperties
{
    @JsonProperty("head_block_number")
    private UInteger headBlockNumber;

    @JsonProperty("head_block_id")
    private BlockId headBlockId;

    @JsonProperty("time")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime time;

    @JsonProperty("current_witness")
    private String currentWitness;

    @JsonProperty("current_supply")
    private Asset currentSupply;

    @JsonProperty("total_vesting_shares")
    private Asset totalVestingShares;

    @JsonProperty("witness_required_vesting")
    private Asset witnessRequiredVesting;

    @JsonProperty("maximum_block_size")
    private UInteger maximumBlockSize;

    @JsonProperty("current_aslot")
    private BigInteger currentAslot; // uint64_t

    @JsonProperty("recent_slots_filled")
    private BigInteger recentSlotsFilled; // uint128_t

    @JsonProperty("participation_count")
    private UByte participationCount;

    @JsonProperty("last_irreversible_block_num")
    private UInteger lastIrreversibleBlockNumber;

    @JsonProperty("average_block_size")
    private Integer averageBlockSize;

    public UInteger getHeadBlockNumber()
    {
        return headBlockNumber;
    }

    public BlockId getHeadBlockId()
    {
        return headBlockId;
    }

    public LocalDateTime getTime()
    {
        return time;
    }

    public String getCurrentWitness()
    {
        return currentWitness;
    }

    public Asset getCurrentSupply()
    {
        return currentSupply;
    }

    public Asset getTotalVestingShares()
    {
        return totalVestingShares;
    }

    public Asset getWitnessRequiredVesting()
    {
        return witnessRequiredVesting;
    }

    public UInteger getMaximumBlockSize()
    {
        return maximumBlockSize;
    }

    public BigInteger getCurrentAslot()
    {
        return currentAslot;
    }

    public BigInteger getRecentSlotsFilled()
    {
        return recentSlotsFilled;
    }

    public UByte getParticipationCount()
    {
        return participationCount;
    }

    public UInteger getLastIrreversibleBlockNumber()
    {
        return lastIrreversibleBlockNumber;
    }

    public Integer getAverageBlockSize()
    {
        return averageBlockSize;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof DynamicGlobalProperties)) return false;
        DynamicGlobalProperties that = (DynamicGlobalProperties) o;
        return Objects.equals(headBlockNumber, that.headBlockNumber) &&
                Objects.equals(headBlockId, that.headBlockId) &&
                Objects.equals(time, that.time) &&
                Objects.equals(currentWitness, that.currentWitness) &&
                Objects.equals(currentSupply, that.currentSupply) &&
                Objects.equals(totalVestingShares, that.totalVestingShares) &&
                Objects.equals(witnessRequiredVesting, that.witnessRequiredVesting) &&
                Objects.equals(maximumBlockSize, that.maximumBlockSize) &&
                Objects.equals(currentAslot, that.currentAslot) &&
                Objects.equals(recentSlotsFilled, that.recentSlotsFilled) &&
                Objects.equals(participationCount, that.participationCount) &&
                Objects.equals(lastIrreversibleBlockNumber, that.lastIrreversibleBlockNumber) &&
                Objects.equals(averageBlockSize, that.averageBlockSize);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(headBlockNumber, headBlockId, time, currentWitness, currentSupply, totalVestingShares, witnessRequiredVesting, maximumBlockSize, currentAslot, recentSlotsFilled, participationCount, lastIrreversibleBlockNumber, averageBlockSize);
    }
}
