/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.api.condenser.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UByte;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;

import java.math.BigInteger;
import java.util.List;

/**
 * This object should only be instantiated by Jackson using reflection
 */
@JsonIgnoreProperties({"id"})
public class ApiWitnessScheduleObject
{
    @JsonProperty("current_virtual_time")
    private BigInteger currentVirtualTime;

    @JsonProperty("next_shuffle_block_num")
    private UInteger nextShuffleBlockNumber;

    @JsonProperty("current_shuffled_witnesses")
    private List<String> currentShuffledWitnesses;

    @JsonProperty("num_scheduled_witnesses")
    private UByte numScheduledWitnesses;

    @JsonProperty("top19_weight")
    private UByte top19Weight;

    @JsonProperty("timeshare_weight")
    private UByte timeshareWeight;

    @JsonProperty("witness_pay_normalization_factor")
    private UInteger witnessPayNormalizationFactor;

    @JsonProperty("median_props")
    private LegacyChainProperties medianProps;

    @JsonProperty("majority_version")
    private String majorityVersion;

    @JsonProperty("max_voted_witnesses")
    private UByte maxVotesWitnesses;

    @JsonProperty("max_runner_witnesses")
    private UByte maxRunnerWitnesses;

    @JsonProperty("hardfork_required_witnesses")
    private UByte hardforkRequiredWitnesses;

    public BigInteger getCurrentVirtualTime()
    {
        return currentVirtualTime;
    }

    public UInteger getNextShuffleBlockNumber()
    {
        return nextShuffleBlockNumber;
    }

    public List<String> getCurrentShuffledWitnesses()
    {
        return currentShuffledWitnesses;
    }

    public UByte getNumScheduledWitnesses()
    {
        return numScheduledWitnesses;
    }

    public UByte getTop19Weight()
    {
        return top19Weight;
    }

    public UByte getTimeshareWeight()
    {
        return timeshareWeight;
    }

    public UInteger getWitnessPayNormalizationFactor()
    {
        return witnessPayNormalizationFactor;
    }

    public LegacyChainProperties getMedianProps()
    {
        return medianProps;
    }

    public String getMajorityVersion()
    {
        return majorityVersion;
    }

    public UByte getMaxVotesWitnesses()
    {
        return maxVotesWitnesses;
    }

    public UByte getMaxRunnerWitnesses()
    {
        return maxRunnerWitnesses;
    }

    public UByte getHardforkRequiredWitnesses()
    {
        return hardforkRequiredWitnesses;
    }
}
