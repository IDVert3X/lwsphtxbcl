/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Random;

@JsonPropertyOrder({"jsonrpc", "method", "params", "id"})
public abstract class AbstractRequest
{
    private static final Random RANDOM = new Random();

    private final long id;

    protected AbstractRequest()
    {
        id = RANDOM.nextLong();
    }

    @JsonProperty("jsonrpc")
    public String getJsonRPC()
    {
        return "2.0";
    }

    @JsonProperty("id")
    public long getId()
    {
        return id;
    }

    @JsonIgnore
    protected String getApiPath()
    {
        String[] path = getClass().getPackage().getName().split("\\.");
        if (!isValidRequestClassPath(path))
            throw new RuntimeException("Invalid request class path, " +
                    "must be `[any_package_tree].api.{api_name}.request`, " +
                    "override AbstractRequest#getApiPath() method if needed.");
        return path[path.length - 2] + "_api.";
    }

    /**
     * Verify that the request class extending this abstract class meets the
     * following class path pattern `[any_package_tree].api.{api_name}.request`.
     *
     * @param path Class path
     * @return Whether the class path is valid
     */
    private boolean isValidRequestClassPath(String[] path)
    {
        if (path.length < 3)
            return false;
        if (!path[path.length - 3].equals("api"))
            return false;
        return path[path.length - 1].equals("request");
    }

    @JsonIgnore
    public abstract String getMethodName();

    @JsonProperty("method")
    public String getMethod()
    {
        return getApiPath() + getMethodName();
    }

    @JsonProperty("params")
    public abstract Object getParams();
}
