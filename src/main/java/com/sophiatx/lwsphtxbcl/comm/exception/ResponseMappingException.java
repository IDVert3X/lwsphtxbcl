/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm.exception;

/**
 * When a response object can not be mapped to a certain model defined in this
 * library. This could be caused by the blockchain version incompatibility or
 * by an incorrect model definition - in this case, please fill a bug report.
 */
public class ResponseMappingException extends CommunicationException
{
    public ResponseMappingException(Throwable throwable)
    {
        super(throwable);
    }
}
