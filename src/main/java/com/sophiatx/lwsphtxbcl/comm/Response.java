/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm;

import com.fasterxml.jackson.databind.JsonNode;
import com.sophiatx.lwsphtxbcl.comm.exception.InvalidResponseException;

class Response
{
    private static final String ERROR_FIELD_NAME = "error";
    private static final String ERROR_CODE_FIELD_NAME = "code";
    private static final String ERROR_MESSAGE_FIELD_NAME = "message";
    private static final String ERROR_DATA_FIELD_NAME = "data";
    private static final String ID_FIELD_NAME = "id";
    private static final String RESULT_FIELD_NAME = "result";
    private static final String JSONRPC_FIELD_NAME = "jsonrpc";
    private static final String JSONRPC_VERSION = "2.0";

    private boolean failed;
    private JsonNode responseJson;

    Response(JsonNode responseJson)
    throws InvalidResponseException
    {
        this.responseJson = responseJson;
        if (!isValidJsonRPC20Response())
            throw new InvalidResponseException("Not a JSON-RPC 2.0 compliant response");
        failed = responseJson.has(ERROR_FIELD_NAME);
    }

    /**
     * Returns a content of the result field
     *
     * @return Result JsonNode
     */
    JsonNode getResult()
    {
        return responseJson.get(RESULT_FIELD_NAME);
    }

    /**
     * Returns whether the response has failed ( contains errors ) or not
     *
     * @return Whether the response contains errors
     */
    boolean isFailed()
    {
        return failed;
    }

    JsonNode getError()
    {
        return responseJson.get(ERROR_FIELD_NAME);
    }

    /**
     * Returns an error message received from the blockchain daemon.
     *
     * @return Error message
     */
    String getErrorMessage()
    {
        if (!failed) return null;
        return getError().get(ERROR_MESSAGE_FIELD_NAME).asText();
    }

    /**
     * Compares the given request id to the received response id in this response
     *
     * @param expectedId Expected response ID
     * @return Whether the IDs match
     */
    boolean hasExpectedId(long expectedId)
    {
        JsonNode idField = responseJson.get(ID_FIELD_NAME);
        if (idField == null || idField.isNull())
            return false;
        return idField.asLong() == expectedId;
    }

    private boolean hasValidJsonRPCVersion()
    {
        JsonNode jsonRpcField = responseJson.get(JSONRPC_FIELD_NAME);
        if (jsonRpcField == null || jsonRpcField.isNull())
            return false;
        return jsonRpcField.asText().equals(JSONRPC_VERSION);
    }

    private boolean hasValidErrorObject()
    {
        JsonNode errNode = responseJson.get(ERROR_FIELD_NAME);
        if (errNode == null || errNode.isNull())
            return false;
        JsonNode errCodeNode = errNode.get(ERROR_CODE_FIELD_NAME);
        if (errCodeNode == null || errCodeNode.isNull() || !errCodeNode.isInt())
            return false;
        JsonNode errMsgNode = errNode.get(ERROR_MESSAGE_FIELD_NAME);
        return errMsgNode != null && !errMsgNode.isNull() &&
                !errMsgNode.isArray() && !errMsgNode.isObject();
    }

    private boolean isValidJsonRPC20Response()
    {
        if (!hasValidJsonRPCVersion())
            return false;

        // if the result is present, error must not be
        if (responseJson.has(RESULT_FIELD_NAME))
            return !responseJson.has(ERROR_FIELD_NAME);

        // if the result is not present, there must be a valid error object
        return hasValidErrorObject();
    }
}
