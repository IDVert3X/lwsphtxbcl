/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.ConversionRate;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

@JsonPropertyOrder({"fee", "publisher", "exchangeRate"})
public class FeedPublishOperation extends AbstractFreeOperation
{
    @JsonProperty("publisher")
    private String publisher;
    @JsonProperty("exchange_rate")
    private ConversionRate exchangeRate;

    @JsonCreator
    public FeedPublishOperation(
            @JsonProperty("publisher") String publisher,
            @JsonProperty("exchange_rate") ConversionRate exchangeRate
    )
    {
        verifyNotNull(publisher, exchangeRate);
        verifyPrecondition(exchangeRate.getBase().getSymbol() == AssetSymbol.SPHTX,
                "Fee exchange rate must use SPHTX as a base currency");
        this.publisher = publisher;
        this.exchangeRate = exchangeRate;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.FEED_PUBLISH;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(publisher).append(exchangeRate);
    }

    public String getPublisher()
    {
        return publisher;
    }

    public ConversionRate getExchangeRate()
    {
        return exchangeRate;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof FeedPublishOperation)) return false;
        FeedPublishOperation that = (FeedPublishOperation) o;
        return Objects.equals(publisher, that.publisher) &&
                Objects.equals(exchangeRate, that.exchangeRate);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(publisher, exchangeRate);
    }
}
