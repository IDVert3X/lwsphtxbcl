/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import com.sophiatx.lwsphtxbcl.encoding.Hex;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UShort;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

@JsonIgnoreProperties({"blockId", "blockNumber", "blockNum", "blockPrefix"})
public class BlockId
{
    private String blockId;

    private UInteger blockNumber;
    private UShort blockNum;
    private UInteger blockPrefix;

    @JsonCreator
    public BlockId(String blockId)
    {
        verifyNotNull(blockId);
        verifyPrecondition(Hex.isValid(blockId, 20), "Invalid block ID");
        this.blockId = blockId;
    }

    private UInteger getBlockNumberFromId()
    {
        byte[] numBytes = Hex.toBytes(blockId.substring(0, 8)); // first 4 bytes
        return UInteger.valueOf(
                ByteBuffer.wrap(numBytes).order(ByteOrder.BIG_ENDIAN).getInt()
        );
    }

    private UInteger getBlockPrefixFromId()
    {
        byte[] bytes = Hex.toBytes(blockId);
        return UInteger.valueOf(
                (long) bytes[4] & 0xFF |
                        ((long) bytes[5] & 0xFF) << 8 |
                        ((long) bytes[6] & 0xFF) << 16 |
                        ((long) bytes[7] & 0xFF) << 24
        );
    }

    public UInteger getBlockNumber()
    {
        if (blockNumber == null)
            blockNumber = getBlockNumberFromId();
        return blockNumber;
    }

    public UShort getBlockNum()
    {
        if (blockNum == null)
            blockNum = UShort.valueOf(getBlockNumber().intValue());
        return blockNum;
    }

    public UInteger getBlockPrefix()
    {
        if (blockPrefix == null)
            blockPrefix = getBlockPrefixFromId();
        return blockPrefix;
    }

    @Override
    @JsonValue
    public String toString()
    {
        return blockId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == this) return true;
        if (!(o instanceof BlockId)) return false;
        return blockId.equals(((BlockId) o).blockId);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(blockId);
    }
}
