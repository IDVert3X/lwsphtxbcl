/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.List;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "sender", "recipients", "appId", "json"})
public class CustomJsonOperation extends AbstractOperation
{
    @JsonProperty("sender")
    private String sender;
    @JsonProperty("recipients")
    private List<String> recipients;
    @JsonProperty("app_id")
    private Long appId;
    @JsonProperty("json")
    private String json;

    @JsonCreator
    public CustomJsonOperation(
            @JsonProperty("fee") Asset fee,
            @JsonProperty("sender") String sender,
            @JsonProperty("recipients") List<String> recipients,
            @JsonProperty("app_id") Long appId,
            @JsonProperty("json") String json
    )
    {
        this(sender, recipients, appId, json);
        setFeeExplicitly(fee);
    }

    public CustomJsonOperation(String sender, List<String> recipients,
            Long appId, String json)
    {
        verifyNotNull(sender, recipients, json);
        this.sender = sender;
        this.recipients = recipients;
        this.appId = appId;
        this.json = json;

        // NOTE: this will be changed after the next HF
        if (toBytes().length > 8192)
            throw new RuntimeException("Operation size exceeds limit of 8KiB");
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.CUSTOM_JSON;
    }

    @Override
    public void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(sender).append(recipients).append(appId).append(json);
    }

    @Override
    protected Asset getBaseFee()
    {
        // NOTE: this will be changed after the next HF
        String feeUSD = toBytes().length < 4096 ? "0.01" : "0.02";
        return new Asset(feeUSD, BASE_FEE_SYMBOL);
    }

    public String getSender()
    {
        return sender;
    }

    public List<String> getRecipients()
    {
        return recipients;
    }

    public Long getAppId()
    {
        return appId;
    }

    public String getJson()
    {
        return json;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof CustomJsonOperation)) return false;
        CustomJsonOperation that = (CustomJsonOperation) o;
        return Objects.equals(fee, that.fee) &&
                appId == that.appId &&
                Objects.equals(sender, that.sender) &&
                Objects.equals(recipients, that.recipients) &&
                Objects.equals(json, that.json);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(fee, sender, recipients, appId, json);
    }
}
