/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.sophiatx.lwsphtxbcl.chain.Chains;
import com.sophiatx.lwsphtxbcl.chain.Configuration;
import com.sophiatx.lwsphtxbcl.chain.model.operation.AbstractOperation;
import com.sophiatx.lwsphtxbcl.crypto.CompactSignature;
import com.sophiatx.lwsphtxbcl.crypto.KeyPair;
import com.sophiatx.lwsphtxbcl.crypto.Sha256Hash;
import com.sophiatx.lwsphtxbcl.crypto.Sign;
import com.sophiatx.lwsphtxbcl.encoding.Hex;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.ByteArrayBuilder;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UShort;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.SophiaTX.OBJECT_MAPPER;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

@JsonPropertyOrder({"refBlockNum", "refBlockPrefix", "expiration", "operations",
        "extensions", "signatures"})
public class Transaction implements BinarySerializable
{
    protected static final String KEY_EXPIRATION = "expiration";
    protected static final String KEY_SIGNATURES = "signatures";
    protected static final String KEY_OPERATIONS = "operations";
    protected static final String KEY_EXTENSIONS = "extensions";
    protected static final String KEY_REF_BLOCK_NUM = "ref_block_num";
    protected static final String KEY_REF_BLOCK_PREFIX = "ref_block_prefix";

    // Default expiration time
    private static final long DEFAULT_EXPIRATION_SECONDS = 30;

    @JsonProperty(KEY_REF_BLOCK_NUM)
    protected final UShort refBlockNum;

    @JsonProperty(KEY_REF_BLOCK_PREFIX)
    protected final UInteger refBlockPrefix;

    @JsonProperty(KEY_EXPIRATION)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = Configuration.DATE_FORMAT, timezone = Configuration.TIME_ZONE)
    protected LocalDateTime expiration;

    @JsonProperty(KEY_OPERATIONS)
    protected final List<AbstractOperation> operations;

    // extensions are not being used in the SophiaTX blockchain yet
    @JsonPropertyOrder(KEY_EXTENSIONS)
    protected List<FutureExtension> extensions = new ArrayList<>();

    @JsonProperty(KEY_SIGNATURES)
    protected final List<CompactSignature> signatures = new ArrayList<>();

    @JsonCreator
    public Transaction(
            @JsonProperty(KEY_REF_BLOCK_NUM) UShort refBlockNum,
            @JsonProperty(KEY_REF_BLOCK_PREFIX) UInteger refBlockPrefix,
            @JsonProperty(KEY_EXPIRATION) LocalDateTime expiration,
            @JsonProperty(KEY_OPERATIONS) List<AbstractOperation> operations,
            @JsonProperty(KEY_EXTENSIONS) List<FutureExtension> extensions
    )
    {
        verifyNotNull(refBlockNum, refBlockPrefix, expiration, operations, extensions);
        this.refBlockNum = refBlockNum;
        this.refBlockPrefix = refBlockPrefix;
        this.expiration = expiration;
        this.operations = operations;
        this.extensions = extensions;
    }

    /**
     * Create a new transaction specifying reference block num and prefix
     *
     * @param refBlockNum    Ref. block number
     * @param refBlockPrefix Ref. block prefix
     */
    public Transaction(UShort refBlockNum, UInteger refBlockPrefix)
    {
        verifyNotNull(refBlockNum, refBlockPrefix);
        this.refBlockNum = refBlockNum;
        this.refBlockPrefix = refBlockPrefix;
        this.operations = new ArrayList<>();
        expireIn(DEFAULT_EXPIRATION_SECONDS);
    }

    /**
     * Create a new transaction referencing the given block
     *
     * @param refBlockId Ref. block ID
     */
    public Transaction(BlockId refBlockId)
    {
        verifyNotNull(refBlockId);
        this.refBlockNum = refBlockId.getBlockNum();
        this.refBlockPrefix = refBlockId.getBlockPrefix();
        this.operations = new ArrayList<>();
        expireIn(DEFAULT_EXPIRATION_SECONDS);
    }

    /**
     * Set a relative expiration time ( in seconds ) from now
     *
     * @param offsetSeconds The time ( in seconds ) in which the TX will expire
     * @return Transaction
     */
    public Transaction expireIn(long offsetSeconds)
    {
        this.expiration = LocalDateTime.ofInstant(
                Instant.now().plusSeconds(offsetSeconds),
                ZoneId.of("UTC")
        );
        return this;
    }

    /**
     * Set an absolute expiration time
     *
     * @param expiration The absolute date & time when the TX will expire
     * @return Transaction
     */
    public Transaction expireAt(LocalDateTime expiration)
    {
        verifyNotNull(expiration);
        this.expiration = expiration;
        return this;
    }

    /**
     * Add an operation to the transaction
     *
     * @param op Operation
     * @return Transaction
     */
    public Transaction addOperation(AbstractOperation op)
    {
        verifyNotNull(op);
        this.operations.add(op);
        return this;
    }

    /**
     * Calculate fees for all the operations included in the transaction
     *
     * @param conversionRate Conversion rate between SPHTX and USD
     * @return Transaction
     */
    public Transaction calculateFees(ConversionRate conversionRate)
    {
        verifyNotNull(conversionRate);

        for (AbstractOperation o : operations)
            o.calculateFee(conversionRate);

        return this;
    }

    /**
     * Compute a transaction digest ( transaction ID ) for a particular chain ID
     *
     * @param chainId ID of a chain the transaction is valid for
     * @return TX digest
     */
    @JsonIgnore
    public Sha256Hash getDigest(String chainId)
    {
        verifyPrecondition(Hex.isValid(chainId, 32), "Invalid chain ID format");
        return Sha256Hash.of(BinUtils.concat(Hex.toBytes(chainId), toBytes()));
    }

    /**
     * Compute a transaction digest ( transaction ID ) for main net
     *
     * @return TX digest
     */
    @JsonIgnore
    public Sha256Hash getDigest()
    {
        return getDigest(Chains.MAIN_NET);
    }

    /**
     * Sign a transaction
     *
     * @param kp      KeyPair of the signing account
     * @param chainId ID of a chain the transaction is valid for
     * @return Signed transaction
     */
    public SignedTransaction sign(KeyPair kp, String chainId)
    {
        verifyNotNull(kp, chainId);
        return new SignedTransaction(this, signTransaction(this, kp, chainId));
    }

    /**
     * Sign a transaction valid on a main net
     *
     * @param kp KeyPair of the signing account
     * @return Signed transaction
     */
    public SignedTransaction sign(KeyPair kp)
    {
        return sign(kp, Chains.MAIN_NET);
    }

    protected static CompactSignature signTransaction(Transaction tx, KeyPair kp, String chainId)
    {
        return Sign.signDigest(tx.getDigest(chainId).getBytes(), kp);
    }

    /**
     * Serialize the transaction to its binary representation
     *
     * @return Binary
     */
    @Override
    public byte[] toBytes()
    {
        ByteArrayBuilder bytes = new ByteArrayBuilder();
        bytes.append(BinUtils.toBytes(refBlockNum));
        bytes.append(BinUtils.toBytes(refBlockPrefix));
        bytes.append(BinUtils.toBytes((int) expiration.toEpochSecond(ZoneOffset.UTC)));
        bytes.append((byte) operations.size());

        for (AbstractOperation operation : operations) {
            bytes.append(BinUtils.toLittleEndian(
                    (byte) operation.getOperationType().ordinal()
            ));
            bytes.append(BinUtils.toBytes(operation));
        }

        bytes.append(BinUtils.toBytes(extensions));

        return bytes.getBytes();
    }

    public UShort getRefBlockNum()
    {
        return refBlockNum;
    }

    public UInteger getRefBlockPrefix()
    {
        return refBlockPrefix;
    }

    public LocalDateTime getExpiration()
    {
        return expiration;
    }

    public List<AbstractOperation> getOperations()
    {
        return operations;
    }

    public List<FutureExtension> getExtensions()
    {
        return extensions;
    }

    @JsonIgnore
    public String getId()
    {
        if (this.operations.size() < 1)
            throw new IllegalStateException("At least 1 operation is needed");
        return Sha256Hash.of(toBytes()).toString().substring(0, 40);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == this) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(refBlockNum, that.refBlockNum) &&
                Objects.equals(refBlockPrefix, that.refBlockPrefix) &&
                Objects.equals(expiration, that.expiration) &&
                Objects.equals(operations, that.operations) &&
                Objects.equals(extensions, that.extensions);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(refBlockNum, refBlockPrefix, expiration, operations, extensions);
    }

    @Override
    public String toString()
    {
        try {
            return OBJECT_MAPPER.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Unable to serialize the transaction", e);
        }
    }
}
