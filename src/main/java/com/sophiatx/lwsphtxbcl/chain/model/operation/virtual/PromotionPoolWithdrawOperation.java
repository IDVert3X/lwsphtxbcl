/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation.virtual;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "toAccount", "withdrawn"})
public class PromotionPoolWithdrawOperation extends AbstractVirtualOperation
{
    @JsonProperty("to_account")
    private String toAccount;
    @JsonProperty("withdrawn")
    private Asset withdrawn;

    @JsonCreator
    public PromotionPoolWithdrawOperation(
            @JsonProperty("to_account") String toAccount,
            @JsonProperty("withdrawn") Asset withdrawn
    )
    {
        verifyNotNull(toAccount, withdrawn);
        this.toAccount = toAccount;
        this.withdrawn = withdrawn;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.PROMOTION_POOL_WITHDRAW;
    }

    public String getToAccount()
    {
        return toAccount;
    }

    public Asset getWithdrawn()
    {
        return withdrawn;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof PromotionPoolWithdrawOperation)) return false;
        PromotionPoolWithdrawOperation that = (PromotionPoolWithdrawOperation) o;
        return Objects.equals(fee, that.fee) &&
                Objects.equals(toAccount, that.toAccount) &&
                Objects.equals(withdrawn, that.withdrawn);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(fee, toAccount, withdrawn);
    }
}
