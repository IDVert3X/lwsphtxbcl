/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.ByteArrayBuilder;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UShort;

import java.util.List;
import java.util.Objects;

public class Authority implements BinarySerializable
{
    @JsonProperty("weight_threshold")
    private UInteger weightThreshold;

    @JsonProperty("account_auths")
    private List<AccountAuthority> accountAuths;

    @JsonProperty("key_auths")
    private List<KeyAuthority> keyAuths;

    @JsonCreator
    public Authority(
            @JsonProperty("weight_threshold") UInteger weightThreshold,
            @JsonProperty("account_auths") List<AccountAuthority> accountAuths,
            @JsonProperty("key_auths") List<KeyAuthority> keyAuths
    )
    {
        this.weightThreshold = weightThreshold;
        this.accountAuths = accountAuths;
        this.keyAuths = keyAuths;
    }

    @Override
    public byte[] toBytes()
    {
        ByteArrayBuilder bytes = new ByteArrayBuilder();
        bytes.append(BinUtils.toBytes(weightThreshold));
        bytes.append(BinUtils.toBytes(accountAuths));
        bytes.append(BinUtils.toBytes(keyAuths));
        return bytes.getBytes();
    }

    public UInteger getWeightThreshold()
    {
        return weightThreshold;
    }

    public List<AccountAuthority> getAccountAuths()
    {
        return accountAuths;
    }

    public List<KeyAuthority> getKeyAuths()
    {
        return keyAuths;
    }

    @JsonFormat(shape = JsonFormat.Shape.ARRAY)
    @JsonPropertyOrder({"accountName", "weight"})
    public static class AccountAuthority implements BinarySerializable
    {
        private String accountName;
        private UShort weight;

        @JsonCreator
        public AccountAuthority(
                @JsonProperty("accountName") String accountName,
                @JsonProperty("weight") UShort weight
        )
        {
            this.accountName = accountName;
            this.weight = weight;
        }

        @Override
        public byte[] toBytes()
        {
            ByteArrayBuilder bytes = new ByteArrayBuilder();
            bytes.append(BinUtils.toBytes(accountName, true));
            bytes.append(BinUtils.toBytes(weight));
            return bytes.getBytes();
        }

        public String getAccountName()
        {
            return accountName;
        }

        public UShort getWeight()
        {
            return weight;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (!(o instanceof AccountAuthority)) return false;
            AccountAuthority that = (AccountAuthority) o;
            return Objects.equals(accountName, that.accountName) &&
                    Objects.equals(weight, that.weight);
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(accountName, weight);
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.ARRAY)
    @JsonPropertyOrder({"publicKey", "weight"})
    public static class KeyAuthority implements BinarySerializable
    {
        private PublicKey publicKey;
        private UShort weight;

        @JsonCreator
        public KeyAuthority(
                @JsonProperty("publicKey") PublicKey publicKey,
                @JsonProperty("weight") UShort weight
        )
        {
            this.publicKey = publicKey;
            this.weight = weight;
        }

        @Override
        public byte[] toBytes()
        {
            ByteArrayBuilder bytes = new ByteArrayBuilder();
            bytes.append(BinUtils.toBytes(publicKey));
            bytes.append(BinUtils.toBytes(weight));
            return bytes.getBytes();
        }

        public PublicKey getPublicKey()
        {
            return publicKey;
        }

        public UShort getWeight()
        {
            return weight;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (!(o instanceof KeyAuthority)) return false;
            KeyAuthority that = (KeyAuthority) o;
            return Objects.equals(publicKey, that.publicKey) &&
                    Objects.equals(weight, that.weight);
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(publicKey, weight);
        }
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof Authority)) return false;
        Authority authority = (Authority) o;
        return Objects.equals(weightThreshold, authority.weightThreshold) &&
                Objects.equals(accountAuths, authority.accountAuths) &&
                Objects.equals(keyAuths, authority.keyAuths);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(weightThreshold, accountAuths, keyAuths);
    }
}
