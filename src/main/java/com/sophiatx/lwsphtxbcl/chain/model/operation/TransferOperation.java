/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

@JsonPropertyOrder({"fee", "from", "to", "amount", "memo"})
public class TransferOperation extends AbstractOperation
{
    @JsonProperty("from")
    private String from;
    @JsonProperty("to")
    private String to;
    @JsonProperty("amount")
    private Asset amount;
    @JsonProperty("memo")
    private String memo;

    @JsonCreator
    public TransferOperation(
            @JsonProperty("fee") Asset fee,
            @JsonProperty("from") String from,
            @JsonProperty("to") String to,
            @JsonProperty("amount") Asset amount,
            @JsonProperty("memo") String memo
    )
    {
        this(from, to, amount, memo);
        setFeeExplicitly(fee);
    }

    public TransferOperation(String from, String to, Asset amount, String memo)
    {
        verifyNotNull(from, to, amount);
        verifyPrecondition(!from.equals(to), "Sender and recipient accounts must differ");
        verifyPrecondition(amount.getAmount().signum() > 0, "Amount must be > 0");
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.memo = memo;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.TRANSFER;
    }

    @Override
    public void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(from).append(to).append(amount).append(memo);
    }

    public String getFrom()
    {
        return from;
    }

    public String getTo()
    {
        return to;
    }

    public Asset getAmount()
    {
        return amount;
    }

    public String getMemo()
    {
        return memo;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof TransferOperation)) return false;
        TransferOperation that = (TransferOperation) o;
        return Objects.equals(that.fee, fee) &&
                Objects.equals(that.from, from) &&
                Objects.equals(that.to, to) &&
                Objects.equals(that.amount, amount) &&
                Objects.equals(that.memo, memo);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(fee, from, to, amount, memo);
    }
}
