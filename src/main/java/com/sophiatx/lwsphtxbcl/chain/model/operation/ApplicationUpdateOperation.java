/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UByte;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "author", "newAuthor", "name", "url", "metadata", "priceParam"})
public class ApplicationUpdateOperation extends AbstractOperation
{
    @JsonProperty("author")
    private String author;
    @JsonProperty("new_author")
    private String newAuthor;
    @JsonProperty("name")
    private String name;
    @JsonProperty("url")
    private String url;
    @JsonProperty("metadata")
    private String metadata;
    @JsonProperty("price_param")
    private UByte priceParam;

    @JsonCreator
    public ApplicationUpdateOperation(
            @JsonProperty("fee") Asset fee,
            @JsonProperty("author") String author,
            @JsonProperty("new_author") String newAuthor,
            @JsonProperty("name") String name,
            @JsonProperty("url") String url,
            @JsonProperty("metadata") String metadata,
            @JsonProperty("price_param") UByte priceParam
    )
    {
        this(author, newAuthor, name, url, metadata, priceParam);
        setFeeExplicitly(fee);
    }

    public ApplicationUpdateOperation(String author, String newAuthor, String name,
            String url, String metadata, UByte priceParam)
    {
        verifyNotNull(author, name);
        this.author = author;
        this.newAuthor = newAuthor;
        this.name = name;
        this.url = url;
        this.metadata = metadata;
        this.priceParam = priceParam;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.APPLICATION_UPDATE;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(author).append(newAuthor).append(name).append(url)
                .append(metadata).append(priceParam);
    }

    public String getAuthor()
    {
        return author;
    }

    public String getNewAuthor()
    {
        return newAuthor;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }

    public String getMetadata()
    {
        return metadata;
    }

    public UByte getPriceParam()
    {
        return priceParam;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof ApplicationUpdateOperation)) return false;
        ApplicationUpdateOperation that = (ApplicationUpdateOperation) o;
        return Objects.equals(fee, that.fee) &&
                Objects.equals(author, that.author) &&
                Objects.equals(newAuthor, that.newAuthor) &&
                Objects.equals(name, that.name) &&
                Objects.equals(url, that.url) &&
                Objects.equals(metadata, that.metadata) &&
                Objects.equals(priceParam, that.priceParam);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(fee, author, newAuthor, name, url, metadata, priceParam);
    }
}
