/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "buyer", "appId"})
public class BuyApplicationOperation extends AbstractOperation
{
    @JsonProperty("buyer")
    private String buyer;
    @JsonProperty("app_id")
    private Long appId;

    @JsonCreator
    public BuyApplicationOperation(
            @JsonProperty("fee") Asset fee,
            @JsonProperty("buyer") String buyer,
            @JsonProperty("app_id") Long appId
    )
    {
        this(buyer, appId);
        setFeeExplicitly(fee);
    }

    public BuyApplicationOperation(String buyer, Long appId)
    {
        verifyNotNull(buyer, appId);
        this.buyer = buyer;
        this.appId = appId;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.BUY_APPLICATION;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(buyer).append(appId);
    }

    public String getBuyer()
    {
        return buyer;
    }

    public Long getAppId()
    {
        return appId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof BuyApplicationOperation)) return false;
        BuyApplicationOperation that = (BuyApplicationOperation) o;
        return Objects.equals(fee, that.fee) &&
                Objects.equals(buyer, that.buyer) &&
                Objects.equals(appId, that.appId);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(fee, buyer, appId);
    }
}
