/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

@JsonPropertyOrder({"fee", "account", "vestingShares"})
public class WithdrawVestingOperation extends AbstractFreeOperation
{
    @JsonProperty("account")
    private String account;
    @JsonProperty("vesting_shares")
    private Asset vestingShares;

    @JsonCreator
    public WithdrawVestingOperation(
            @JsonProperty("account") String account,
            @JsonProperty("vesting_shares") Asset vestingShares
    )
    {
        verifyNotNull(account, vestingShares);
        verifyPrecondition(vestingShares.getSymbol() == AssetSymbol.VESTS,
                "Only VESTS can be withdrawn");
        this.account = account;
        this.vestingShares = vestingShares;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.WITHDRAW_VESTING;
    }

    @Override
    public void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(account).append(vestingShares);
    }

    public String getAccount()
    {
        return account;
    }

    public Asset getVestingShares()
    {
        return vestingShares;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof WithdrawVestingOperation)) return false;
        WithdrawVestingOperation that = (WithdrawVestingOperation) o;
        return Objects.equals(account, that.account) &&
                Objects.equals(vestingShares, that.vestingShares);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(account, vestingShares);
    }
}
