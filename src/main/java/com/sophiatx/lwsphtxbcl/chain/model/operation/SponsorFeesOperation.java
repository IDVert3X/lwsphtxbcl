/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.FutureExtension;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "sponsor", "sponsored", "isSponsoring"})
public class SponsorFeesOperation extends AbstractFreeOperation
{
    @JsonProperty("sponsor")
    private String sponsor;

    @JsonProperty("sponsored")
    private String sponsored;

    @JsonProperty("is_sponsoring")
    private boolean isSponsoring;

    @JsonProperty("extensions")
    private List<FutureExtension> extensions = new ArrayList<>();

    @JsonCreator
    public SponsorFeesOperation(
            @JsonProperty("sponsor") String sponsor,
            @JsonProperty("sponsored") String sponsored,
            @JsonProperty("is_sponsoring") boolean isSponsoring,
            @JsonProperty("extensions") List<FutureExtension> extensions
    )
    {
        this(sponsor, sponsored, isSponsoring);
        this.extensions = extensions;
    }

    public SponsorFeesOperation(String sponsor, String sponsored, boolean isSponsoring)
    {
        verifyNotNull(sponsor, sponsored);
        this.sponsor = sponsor;
        this.sponsored = sponsored;
        this.isSponsoring = isSponsoring;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.SPONSOR_FEES;
    }

    @Override
    public void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(sponsor).append(sponsored).append(isSponsoring).append(extensions);
    }

    public String getSponsor()
    {
        return sponsor;
    }

    public String getSponsored()
    {
        return sponsored;
    }

    public boolean isSponsoring()
    {
        return isSponsoring;
    }

    public List<FutureExtension> getExtensions()
    {
        return extensions;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof SponsorFeesOperation)) return false;
        SponsorFeesOperation that = (SponsorFeesOperation) o;
        return isSponsoring == that.isSponsoring &&
                Objects.equals(sponsor, that.sponsor) &&
                Objects.equals(sponsored, that.sponsored) &&
                Objects.equals(extensions, that.extensions);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(sponsor, sponsored, isSponsoring, extensions);
    }
}
