/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.chain.model.ConversionRate;
import com.sophiatx.lwsphtxbcl.chain.model.operation.virtual.FillVestingWithdrawOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.virtual.HardforkOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.virtual.InterestOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.virtual.ProducerRewardOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.virtual.PromotionPoolWithdrawOperation;
import com.sophiatx.lwsphtxbcl.chain.model.operation.virtual.ShutdownWitnessOperation;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.WRAPPER_ARRAY)
@JsonSubTypes({
        @Type(value = TransferOperation.class, name = "transfer"),
        @Type(value = TransferToVestingOperation.class, name = "transfer_to_vesting"),
        @Type(value = WithdrawVestingOperation.class, name = "withdraw_vesting"),
        @Type(value = FeedPublishOperation.class, name = "feed_publish"),
        @Type(value = AccountCreateOperation.class, name = "account_create"),
        @Type(value = AccountUpdateOperation.class, name = "account_update"),
        @Type(value = AccountDeleteOperation.class, name = "account_delete"),
        @Type(value = WitnessUpdateOperation.class, name = "witness_update"),
        @Type(value = WitnessStopOperation.class, name = "witness_stop"),
        @Type(value = AccountWitnessVoteOperation.class, name = "account_witness_vote"),
        @Type(value = AccountWitnessProxyOperation.class, name = "account_witness_proxy"),
        @Type(value = WitnessSetPropertiesOperation.class, name = "witness_set_properties"),
        @Type(value = CustomJsonOperation.class, name = "custom_json"),
        @Type(value = CustomBinaryOperation.class, name = "custom_binary"),
        @Type(value = RequestAccountRecovery.class, name = "request_account_recovery"),
        @Type(value = RecoverAccountOperation.class, name = "recover_account"),
        @Type(value = ChangeRecoveryAccountOperation.class, name = "change_recovery_account"),
        @Type(value = EscrowTransferOperation.class, name = "escrow_transfer"),
        @Type(value = EscrowDisputeOperation.class, name = "escrow_dispute"),
        @Type(value = EscrowApproveOperation.class, name = "escrow_approve"),
        @Type(value = ResetAccountOperation.class, name = "reset_account"),
        @Type(value = SetResetAccountOperation.class, name = "set_reset_account"),
        @Type(value = ApplicationCreateOperation.class, name = "application_create"),
        @Type(value = ApplicationUpdateOperation.class, name = "application_update"),
        @Type(value = ApplicationDeleteOperation.class, name = "application_delete"),
        @Type(value = BuyApplicationOperation.class, name = "buy_application"),
        @Type(value = CancelApplicationBuyingOperation.class, name = "cancel_application_buying"),
        @Type(value = TransferFromPromotionPoolOperation.class, name = "transfer_from_promotion_pool"),
        @Type(value = SponsorFeesOperation.class, name = "sponsor_fees"),
        @Type(value = InterestOperation.class, name = "interest"),
        @Type(value = FillVestingWithdrawOperation.class, name = "fill_vesting_withdraw"),
        @Type(value = ShutdownWitnessOperation.class, name = "shutdown_witness"),
        @Type(value = HardforkOperation.class, name = "hardfork"),
        @Type(value = ProducerRewardOperation.class, name = "producer_reward"),
        @Type(value = PromotionPoolWithdrawOperation.class, name = "promotion_pool_withdraw")
})
abstract public class AbstractOperation implements BinarySerializable
{
    public static final AssetSymbol BASE_FEE_SYMBOL = AssetSymbol.USD;
    public static final Asset BASE_FEE = new Asset("0.01", BASE_FEE_SYMBOL);

    protected Asset fee;

    @JsonIgnore
    public abstract OperationType getOperationType();

    @Override
    public byte[] toBytes()
    {
        ObjectBinaryBuilder bytes = new ObjectBinaryBuilder();
        bytes.append(BinUtils.toBytes(getFee(), 16));
        buildOperationBinary(bytes);
        return bytes.getBytes();
    }

    protected abstract void buildOperationBinary(ObjectBinaryBuilder bytes);

    /**
     * Calculate the actual fee ( in SPHTX ) for the operation
     *
     * @param rate Conversion rate between SPHTX and USD
     */
    public void calculateFee(ConversionRate rate)
    {
        verifyNotNull(rate);

        Asset baseFee = getBaseFee();

        verifyPrecondition(rate.getBase().getSymbol() == AssetSymbol.SPHTX,
                "Expected conversion rate base unit: SPHTX" +
                        ", got: " + rate.getBase().getSymbol().toString());
        verifyPrecondition(rate.getQuote().getSymbol() == baseFee.getSymbol(),
                "Expected conversion rate quote unit: " + baseFee.getSymbol().toString() +
                        ", got: " + rate.getQuote().getSymbol().toString());

        this.fee = rate.convert(baseFee);
    }

    /**
     * Get/calculate operation fee in base unit - the one against which the fee
     * prices are fixed, that's USD in our case.
     *
     * Override this method if operation doesn't have a fixed fee of 0.01 USD.
     * If the operation is free, do not override this method, use
     * {@link AbstractFreeOperation} as a base class instead.
     *
     * @return Operation fee in base unit ( USD )
     */
    protected Asset getBaseFee()
    {
        return BASE_FEE;
    }

    protected void setFeeExplicitly(Asset fee)
    {
        verifyNotNull(fee);
        this.fee = fee;
    }

    public Asset getFee()
    {
        return fee;
    }
}
