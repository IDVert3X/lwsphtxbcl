/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;

import java.time.LocalDateTime;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "from", "to", "agent", "escrowId", "sophiatxAmount", "escrowFee", "jsonMeta"})
public class EscrowTransferOperation extends AbstractOperation
{
    @JsonProperty("from")
    private String from;
    @JsonProperty("to")
    private String to;
    @JsonProperty("agent")
    private String agent;
    @JsonProperty("escrow_id")
    private UInteger escrowId;

    @JsonProperty("sophiatx_amount")
    private Asset sophiatxAmount;
    @JsonProperty("escrow_fee")
    private Asset escrowFee;

    @JsonProperty("ratification_deadline")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime ratificationDeadline;

    @JsonProperty("escrow_expiration")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime escrowExpiration;

    @JsonProperty("json_meta")
    private String jsonMeta;

    @JsonCreator
    public EscrowTransferOperation(
            @JsonProperty("fee") Asset fee,
            @JsonProperty("from") String from,
            @JsonProperty("to") String to,
            @JsonProperty("agent") String agent,
            @JsonProperty("escrow_id") UInteger escrowId,
            @JsonProperty("sophiatx_amount") Asset sophiatxAmount,
            @JsonProperty("escrow_fee") Asset escrowFee,
            @JsonProperty("json_meta") String jsonMeta,
            @JsonProperty("ratification_deadline") LocalDateTime ratificationDeadline,
            @JsonProperty("escrow_expiration") LocalDateTime escrowExpiration
    )
    {
        this(from, to, agent, escrowId, sophiatxAmount, escrowFee, jsonMeta,
                ratificationDeadline, escrowExpiration);
        setFeeExplicitly(fee);
    }

    public EscrowTransferOperation(String from, String to, String agent, UInteger escrowId, Asset sophiatxAmount, Asset escrowFee, String jsonMeta, LocalDateTime ratificationDeadline, LocalDateTime escrowExpiration)
    {
        verifyNotNull(from, to, agent, escrowId, sophiatxAmount, escrowFee,
                ratificationDeadline, escrowExpiration);
        this.from = from;
        this.to = to;
        this.agent = agent;
        this.escrowId = escrowId;
        this.sophiatxAmount = sophiatxAmount;
        this.escrowFee = escrowFee;
        this.jsonMeta = jsonMeta;
        this.ratificationDeadline = ratificationDeadline;
        this.escrowExpiration = escrowExpiration;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.ESCROW_TRANSFER;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(from).append(to).append(agent).append(escrowId)
                .append(sophiatxAmount).append(escrowFee)
                .append(ratificationDeadline).append(escrowExpiration)
                .append(jsonMeta);
    }

    public String getFrom()
    {
        return from;
    }

    public String getTo()
    {
        return to;
    }

    public String getAgent()
    {
        return agent;
    }

    public UInteger getEscrowId()
    {
        return escrowId;
    }

    public Asset getSophiatxAmount()
    {
        return sophiatxAmount;
    }

    public Asset getEscrowFee()
    {
        return escrowFee;
    }

    public String getJsonMeta()
    {
        return jsonMeta;
    }

    public LocalDateTime getRatificationDeadline()
    {
        return ratificationDeadline;
    }

    public LocalDateTime getEscrowExpiration()
    {
        return escrowExpiration;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EscrowTransferOperation that = (EscrowTransferOperation) o;
        return Objects.equals(fee, that.fee) &&
                Objects.equals(from, that.from) &&
                Objects.equals(to, that.to) &&
                Objects.equals(agent, that.agent) &&
                Objects.equals(escrowId, that.escrowId) &&
                Objects.equals(sophiatxAmount, that.sophiatxAmount) &&
                Objects.equals(escrowFee, that.escrowFee) &&
                Objects.equals(jsonMeta, that.jsonMeta) &&
                Objects.equals(ratificationDeadline, that.ratificationDeadline) &&
                Objects.equals(escrowExpiration, that.escrowExpiration);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(fee, from, to, agent, escrowId, sophiatxAmount, escrowFee,
                jsonMeta, ratificationDeadline, escrowExpiration);
    }
}
