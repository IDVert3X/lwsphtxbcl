/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.chain.model.FutureExtension;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.List;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"transferTo", "amount", "extensions"})
public class TransferFromPromotionPoolOperation extends AbstractFreeOperation
{
    @JsonProperty("transfer_to")
    private String transferTo;
    @JsonProperty("amount")
    private Asset amount;
    @JsonProperty("extensions")
    private List<FutureExtension> extensions;

    @JsonCreator
    public TransferFromPromotionPoolOperation(
            @JsonProperty("transfer_to") String transferTo,
            @JsonProperty("amount") Asset amount,
            @JsonProperty("extensions") List<FutureExtension> extensions
    )
    {
        verifyNotNull(transferTo, amount);
        this.transferTo = transferTo;
        this.amount = amount;
        this.extensions = extensions;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.TRANSFER_FROM_PROMOTION_POOL;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(transferTo).append(amount).append(extensions);
    }

    public String getTransferTo()
    {
        return transferTo;
    }

    public Asset getAmount()
    {
        return amount;
    }

    public List<FutureExtension> getExtensions()
    {
        return extensions;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof TransferFromPromotionPoolOperation)) return false;
        TransferFromPromotionPoolOperation that = (TransferFromPromotionPoolOperation) o;
        return Objects.equals(transferTo, that.transferTo) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(extensions, that.extensions);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(transferTo, amount, extensions);
    }
}
