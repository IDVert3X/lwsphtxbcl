/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.ChainProperties;
import com.sophiatx.lwsphtxbcl.chain.model.PublicKey;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "owner", "url", "blockSigningKey", "props"})
public class WitnessUpdateOperation extends AbstractFreeOperation
{
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("url")
    private String url;
    @JsonProperty("block_signing_key")
    private PublicKey blockSigningKey;
    @JsonProperty("props")
    private ChainProperties props;

    public WitnessUpdateOperation(
            @JsonProperty("owner") String owner,
            @JsonProperty("url") String url,
            @JsonProperty("block_signing_key") PublicKey blockSigningKey,
            @JsonProperty("props") ChainProperties props
    )
    {
        verifyNotNull(owner);
        this.owner = owner;
        this.url = url;
        this.blockSigningKey = blockSigningKey;
        this.props = props;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.WITNESS_UPDATE;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(owner).append(url).append(blockSigningKey).append(props);
    }

    public String getOwner()
    {
        return owner;
    }

    public String getUrl()
    {
        return url;
    }

    public PublicKey getBlockSigningKey()
    {
        return blockSigningKey;
    }

    public ChainProperties getProps()
    {
        return props;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof WitnessUpdateOperation)) return false;
        WitnessUpdateOperation that = (WitnessUpdateOperation) o;
        return Objects.equals(owner, that.owner) &&
                Objects.equals(url, that.url) &&
                Objects.equals(blockSigningKey, that.blockSigningKey) &&
                Objects.equals(props, that.props);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(owner, url, blockSigningKey, props);
    }
}
