/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import com.sophiatx.lwsphtxbcl.crypto.Keys;
import com.sophiatx.lwsphtxbcl.encoding.KeyEncoder;
import com.sophiatx.lwsphtxbcl.encoding.exception.KeyFormatException;
import com.sophiatx.lwsphtxbcl.utils.BinUtils;
import com.sophiatx.lwsphtxbcl.utils.interfaces.BinarySerializable;

import java.math.BigInteger;

@JsonIgnoreProperties({"key", "wif"})
public class PrivateKey implements BinarySerializable
{
    private final BigInteger key;
    private String wif;

    public PrivateKey(BigInteger key)
    {
        this.key = key;
    }

    @JsonCreator
    public PrivateKey(String key)
    throws KeyFormatException
    {
        this.key = KeyEncoder.decodeWif(key);
        this.wif = key;
    }

    public BigInteger getKey()
    {
        return key;
    }

    public PublicKey derivePublicKey()
    {
        return new PublicKey(Keys.publicKeyFromPrivate(key));
    }

    @Override
    @JsonValue
    public String toString()
    {
        if (wif == null)
            this.wif = KeyEncoder.encodeWif(key);
        return wif;
    }

    @Override
    public byte[] toBytes()
    {
        return BinUtils.toBytesPadded(key, 32);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof PrivateKey)) return false;
        return key.equals(((PrivateKey) o).getKey());
    }

    @Override
    public int hashCode()
    {
        return key.hashCode();
    }
}
