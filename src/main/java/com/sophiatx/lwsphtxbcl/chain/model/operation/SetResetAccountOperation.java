/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "account", "currentResetAccount", "resetAccount"})
public class SetResetAccountOperation extends AbstractFreeOperation
{
    @JsonProperty("account")
    private String account;
    @JsonProperty("current_reset_account")
    private String currentResetAccount;
    @JsonProperty("reset_account")
    private String resetAccount;

    public SetResetAccountOperation(
            @JsonProperty("account") String account,
            @JsonProperty("current_reset_account") String currentResetAccount,
            @JsonProperty("reset_account") String resetAccount
    )
    {
        verifyNotNull(account, currentResetAccount, resetAccount);
        this.account = account;
        this.currentResetAccount = currentResetAccount;
        this.resetAccount = resetAccount;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.SET_RESET_ACCOUNT;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(account).append(currentResetAccount).append(resetAccount);
    }

    public String getAccount()
    {
        return account;
    }

    public String getCurrentResetAccount()
    {
        return currentResetAccount;
    }

    public String getResetAccount()
    {
        return resetAccount;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof SetResetAccountOperation)) return false;
        SetResetAccountOperation that = (SetResetAccountOperation) o;
        return Objects.equals(account, that.account) &&
                Objects.equals(currentResetAccount, that.currentResetAccount) &&
                Objects.equals(resetAccount, that.resetAccount);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(account, currentResetAccount, resetAccount);
    }
}
