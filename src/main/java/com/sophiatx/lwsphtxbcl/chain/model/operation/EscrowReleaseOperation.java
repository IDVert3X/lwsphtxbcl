/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"from", "to", "agent", "who", "receiver", "escrowId", "sophiatxAmount"})
public class EscrowReleaseOperation extends AbstractOperation
{
    @JsonProperty("from")
    private String from;
    @JsonProperty("to")
    private String to;
    @JsonProperty("agent")
    private String agent;
    @JsonProperty("who")
    private String who;
    @JsonProperty("receiver")
    private String receiver;
    @JsonProperty("escrow_id")
    private UInteger escrowId;
    @JsonProperty("sophiatx_amount")
    private Asset sophiatxAmount;

    @JsonCreator
    public EscrowReleaseOperation(
            @JsonProperty("fee") Asset fee,
            @JsonProperty("from") String from,
            @JsonProperty("to") String to,
            @JsonProperty("agent") String agent,
            @JsonProperty("who") String who,
            @JsonProperty("receiver") String receiver,
            @JsonProperty("escrow_id") UInteger escrowId,
            @JsonProperty("sophiatx_amount") Asset sophiatxAmount
    )
    {
        verifyNotNull(from, to, agent, who, receiver, escrowId, sophiatxAmount);
        setFeeExplicitly(fee);
        this.from = from;
        this.to = to;
        this.agent = agent;
        this.who = who;
        this.receiver = receiver;
        this.escrowId = escrowId;
        this.sophiatxAmount = sophiatxAmount;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.ESCROW_RELEASE;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(from).append(to).append(agent).append(who).append(receiver)
                .append(escrowId).append(sophiatxAmount);
    }

    public String getFrom()
    {
        return from;
    }

    public String getTo()
    {
        return to;
    }

    public String getAgent()
    {
        return agent;
    }

    public String getWho()
    {
        return who;
    }

    public String getReceiver()
    {
        return receiver;
    }

    public UInteger getEscrowId()
    {
        return escrowId;
    }

    public Asset getSophiatxAmount()
    {
        return sophiatxAmount;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EscrowReleaseOperation that = (EscrowReleaseOperation) o;
        return Objects.equals(from, that.from) &&
                Objects.equals(to, that.to) &&
                Objects.equals(agent, that.agent) &&
                Objects.equals(who, that.who) &&
                Objects.equals(receiver, that.receiver) &&
                Objects.equals(escrowId, that.escrowId) &&
                Objects.equals(sophiatxAmount, that.sophiatxAmount);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(from, to, agent, who, receiver, escrowId, sophiatxAmount);
    }
}
