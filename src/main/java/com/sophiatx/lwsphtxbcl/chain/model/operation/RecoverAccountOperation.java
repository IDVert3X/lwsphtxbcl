/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Authority;
import com.sophiatx.lwsphtxbcl.chain.model.FutureExtension;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.List;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "accountToRecover", "newOwnerAuthority", "recentOwnerAuthority", "extensions"})
public class RecoverAccountOperation extends AbstractFreeOperation
{
    @JsonProperty("account_to_recover")
    private String accountToRecover;
    @JsonProperty("new_owner_authority")
    private Authority newOwnerAuthority;
    @JsonProperty("recent_owner_authority")
    private Authority recentOwnerAuthority;
    @JsonProperty("extensions")
    private List<FutureExtension> extensions;

    @JsonCreator
    public RecoverAccountOperation(
            @JsonProperty("account_to_recover") String accountToRecover,
            @JsonProperty("new_owner_authority") Authority newOwnerAuthority,
            @JsonProperty("recent_owner_authority") Authority recentOwnerAuthority,
            @JsonProperty("extensions") List<FutureExtension> extensions
    )
    {
        this(accountToRecover, newOwnerAuthority, recentOwnerAuthority);
        this.extensions = extensions;
    }

    public RecoverAccountOperation(
            String accountToRecover,
            Authority newOwnerAuthority,
            Authority recentOwnerAuthority
    )
    {
        verifyNotNull(accountToRecover, newOwnerAuthority, recentOwnerAuthority);
        this.accountToRecover = accountToRecover;
        this.newOwnerAuthority = newOwnerAuthority;
        this.recentOwnerAuthority = recentOwnerAuthority;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.RECOVER_ACCOUNT;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(accountToRecover).append(newOwnerAuthority)
                .append(recentOwnerAuthority).append(extensions);
    }

    public String getAccountToRecover()
    {
        return accountToRecover;
    }

    public Authority getNewOwnerAuthority()
    {
        return newOwnerAuthority;
    }

    public Authority getRecentOwnerAuthority()
    {
        return recentOwnerAuthority;
    }

    public List<FutureExtension> getExtensions()
    {
        return extensions;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof RecoverAccountOperation)) return false;
        RecoverAccountOperation that = (RecoverAccountOperation) o;
        return Objects.equals(accountToRecover, that.accountToRecover) &&
                Objects.equals(newOwnerAuthority, that.newOwnerAuthority) &&
                Objects.equals(recentOwnerAuthority, that.recentOwnerAuthority) &&
                Objects.equals(extensions, that.extensions);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(accountToRecover, newOwnerAuthority, recentOwnerAuthority, extensions);
    }
}
