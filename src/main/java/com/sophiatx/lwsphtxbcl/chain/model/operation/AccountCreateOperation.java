/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.chain.model.Authority;
import com.sophiatx.lwsphtxbcl.chain.model.ConversionRate;
import com.sophiatx.lwsphtxbcl.chain.model.PublicKey;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "creator", "nameSeed", "owner", "active", "memoKey", "jsonMetadata"})
public class AccountCreateOperation extends AbstractOperation
{
    @JsonProperty("creator")
    private String creator;
    @JsonProperty("name_seed")
    private String nameSeed;
    @JsonProperty("owner")
    private Authority owner;
    @JsonProperty("active")
    private Authority active;
    @JsonProperty("memo_key")
    private PublicKey memoKey;
    @JsonProperty("json_metadata")
    private String jsonMetadata;

    @JsonCreator
    public AccountCreateOperation(
            @JsonProperty("fee") Asset fee,
            @JsonProperty("creator") String creator,
            @JsonProperty("name_seed") String nameSeed,
            @JsonProperty("owner") Authority owner,
            @JsonProperty("active") Authority active,
            @JsonProperty("memo_key") PublicKey memoKey,
            @JsonProperty("json_metadata") String jsonMetadata
    )
    {
        verifyNotNull(creator, nameSeed, owner, active, memoKey);
        setFeeExplicitly(fee);
        this.creator = creator;
        this.nameSeed = nameSeed;
        this.owner = owner;
        this.active = active;
        this.memoKey = memoKey;
        this.jsonMetadata = jsonMetadata;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.ACCOUNT_CREATE;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(creator).append(nameSeed).append(owner).append(active)
                .append(memoKey).append(jsonMetadata);
    }

    @Override
    public void calculateFee(ConversionRate rate)
    {
        // fee for this operation can not be calculated, it must be passed to
        // the constructor. This fee is set by the witnesses and can be obtained
        // from ChainProperties object.
    }

    public String getCreator()
    {
        return creator;
    }

    public String getNameSeed()
    {
        return nameSeed;
    }

    public Authority getOwner()
    {
        return owner;
    }

    public Authority getActive()
    {
        return active;
    }

    public PublicKey getMemoKey()
    {
        return memoKey;
    }

    public String getJsonMetadata()
    {
        return jsonMetadata;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof AccountCreateOperation)) return false;
        AccountCreateOperation that = (AccountCreateOperation) o;
        return Objects.equals(creator, that.creator) &&
                Objects.equals(nameSeed, that.nameSeed) &&
                Objects.equals(owner, that.owner) &&
                Objects.equals(active, that.active) &&
                Objects.equals(memoKey, that.memoKey) &&
                Objects.equals(jsonMetadata, that.jsonMetadata);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(creator, nameSeed, owner, active, memoKey, jsonMetadata);
    }
}
