/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.FutureExtension;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;
import com.sophiatx.lwsphtxbcl.utils.conversion.FlatMapDeserializer;
import com.sophiatx.lwsphtxbcl.utils.conversion.FlatMapSerializer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;

@JsonPropertyOrder({"fee", "owner", "props", "extensions"})
public class WitnessSetPropertiesOperation extends AbstractFreeOperation
{
    @JsonProperty("owner")
    private String owner;

    @JsonProperty("props")
    @JsonSerialize(using = FlatMapSerializer.class)
    @JsonDeserialize(
            using = FlatMapDeserializer.class,
            keyAs = String.class, contentAs = String.class
    )
    private Map<String, String> props; // orig. type: flat_map<string,vector<char>>

    @JsonProperty("extensions")
    private List<FutureExtension> extensions = new ArrayList<>();

    @JsonCreator
    public WitnessSetPropertiesOperation(
            @JsonProperty("owner") String owner,
            @JsonProperty("props") Map<String, String> props,
            @JsonProperty("extensions") List<FutureExtension> extensions
    )
    {
        this(owner, props);
        this.extensions = extensions;
    }

    public WitnessSetPropertiesOperation(String owner, Map<String, String> props)
    {
        verifyNotNull(owner);
        this.owner = owner;
        this.props = props;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.WITNESS_SET_PROPERTIES;
    }

    @Override
    protected void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(owner).append(props).append(extensions);
    }

    public String getOwner()
    {
        return owner;
    }

    public Map<String, String> getProps()
    {
        return props;
    }

    public List<FutureExtension> getExtensions()
    {
        return extensions;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof WitnessSetPropertiesOperation)) return false;
        WitnessSetPropertiesOperation that = (WitnessSetPropertiesOperation) o;
        return Objects.equals(owner, that.owner) &&
                Objects.equals(props, that.props) &&
                Objects.equals(extensions, that.extensions);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(owner, props, extensions);
    }
}
