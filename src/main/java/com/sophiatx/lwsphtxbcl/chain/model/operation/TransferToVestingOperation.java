/*
 * Copyright 2018-2019 TX Technologies AG, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.enums.OperationType;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.utils.ObjectBinaryBuilder;

import java.util.Objects;

import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyNotNull;
import static com.sophiatx.lwsphtxbcl.utils.ValidationUtils.verifyPrecondition;

@JsonPropertyOrder({"fee", "from", "to", "amount"})
public class TransferToVestingOperation extends AbstractFreeOperation
{
    @JsonProperty("from")
    private String from;
    @JsonProperty("to")
    private String to;
    @JsonProperty("amount")
    private Asset amount;

    @JsonCreator
    public TransferToVestingOperation(
            @JsonProperty("from") String from,
            @JsonProperty("to") String to,
            @JsonProperty("amount") Asset amount
    )
    {
        verifyNotNull(from, amount);
        verifyPrecondition(amount.getSymbol() == AssetSymbol.SPHTX, "Only SPHTX can be vested");
        this.from = from;
        this.to = to == null ? null : to.equals(from) ? null : to;
        this.amount = amount;
    }

    @Override
    public OperationType getOperationType()
    {
        return OperationType.TRANSFER_TO_VESTING;
    }

    @Override
    public void buildOperationBinary(ObjectBinaryBuilder bytes)
    {
        bytes.append(from).append(to).append(amount);
    }

    public String getFrom()
    {
        return from;
    }

    public String getTo()
    {
        return to;
    }

    public Asset getAmount()
    {
        return amount;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof TransferToVestingOperation)) return false;
        TransferToVestingOperation that = (TransferToVestingOperation) o;
        return Objects.equals(from, that.from) &&
                Objects.equals(to, that.to) &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(from, to, amount);
    }
}
