/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UShort;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BlockIdTests
{
    private static BlockId blockId;

    @BeforeClass
    public static void createBlockId()
    {
        blockId = new BlockId("000fa93e1ca7a040b2d69805ff14ef9bf30609c6");
    }

    @Test
    public void testGetBlockNumber()
    {
        assertEquals(UInteger.valueOf(1026366), blockId.getBlockNumber());
    }

    @Test
    public void testGetBlockNum()
    {
        assertEquals(UShort.valueOf(1026366), blockId.getBlockNum());
    }

    @Test
    public void testGetBlockPrefix()
    {
        assertEquals(UInteger.valueOf(1084270364), blockId.getBlockPrefix());
    }
}
