/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.chain.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UInteger;
import com.sophiatx.lwsphtxbcl.utils.unsigned.UShort;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AuthorityTests
{
    private static final String testJson = "{\"weight_threshold\":1,\"account_auths\":[[\"randomuser\", 1]],\"key_auths\":[[\"SPH6D1rTi6g2WB8V2inFQ8yYa9dxoZ8xU1VN3w28tT1PAQEdwi1hd\",1]]}";

    @Test
    public void testMapAuthority()
    throws IOException
    {
        Authority a = new ObjectMapper().readValue(testJson, Authority.class);

        List<Authority.AccountAuthority> accountAuths = new ArrayList<>();
        accountAuths.add(new Authority.AccountAuthority("randomuser", UShort.valueOf(1)));

        List<Authority.KeyAuthority> keyAuths = new ArrayList<>();
        keyAuths.add(new Authority.KeyAuthority(
                new PublicKey("SPH6D1rTi6g2WB8V2inFQ8yYa9dxoZ8xU1VN3w28tT1PAQEdwi1hd"),
                UShort.valueOf(1)
        ));

        Authority expected = new Authority(UInteger.valueOf(1), accountAuths, keyAuths);

        assertEquals(expected, a);
    }
}
