/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl;

import com.sophiatx.lwsphtxbcl.chain.enums.AssetSymbol;
import com.sophiatx.lwsphtxbcl.chain.model.Asset;
import com.sophiatx.lwsphtxbcl.chain.model.ConversionRate;
import com.sophiatx.lwsphtxbcl.crypto.CompactSignature;
import com.sophiatx.lwsphtxbcl.crypto.KeyPair;
import com.sophiatx.lwsphtxbcl.crypto.Sha256Hash;
import com.sophiatx.lwsphtxbcl.encoding.Hex;
import com.sophiatx.lwsphtxbcl.utils.NumUtils;

import java.io.File;
import java.math.BigInteger;
import java.net.URISyntaxException;

/**
 * Data generated for unit testing purposes
 */
public class Samples
{
    /* KEY PAIR */

    public static final String PRIVATE_KEY_STRING = "5K8nWoti2bZyyouGQTNfPsgVyhGPFRQn5YeZ3kmMya5aybKJQMg";
    public static final BigInteger PRIVATE_KEY = NumUtils.toBigInt("adb332e237f2bb301ebb2ff71fa5f3c188bc50c7edc804b089b0a8131155dace");

    public static final String PUBLIC_KEY_STRING = "SPH7ee3zjWLV5HVNMZkCUZDuXmyNWaWiPD56ffPNofg41ZuxCu42Z";
    public static final BigInteger PUBLIC_KEY = NumUtils.toBigInt("36ba3deefd2fd1047bd598fbb9b09d0a81e9a916db0741ac29800fb3050a9a6c4");

    public static final KeyPair KEY_PAIR = new KeyPair(PRIVATE_KEY, PUBLIC_KEY);

    /* FEE CONVERSION RATE */

    public static final ConversionRate FEE_CONVERSION_RATE = new ConversionRate(new Asset("1", AssetSymbol.SPHTX), new Asset("1", AssetSymbol.USD));

    /* SIGNATURE */

    public static final String SIGNATURE_STR = "1f7333e00d20dcd8ed71fbedc53673ea107967137f7b01d99195e289949ac4f3d34e2f67cd75fb5e1947b15705dac70bcdfa3e76d5d9bedaf747c230c557343ecb";
    public static final CompactSignature SIGNATURE = new CompactSignature(
            0,
            new BigInteger("52107633132501164696119778767725408278347254694072392389386506880777340646355"),
            new BigInteger("35364160420627093297615664141981427386234057401158431940280651035434730929867")
    );

    /* MESSAGE FOR HASH TESTS */

    public static final byte[] MESSAGE = "Don't break it!".getBytes();
    public static final String MESSAGE_HEX = Hex.toString(MESSAGE);
    public static final String MESSAGE_SHA256_HEX = "1839e9364c1df116b9441800832b54b5c1629751a25e49c5e6db30165b4cceec";
    public static final byte[] MESSAGE_SHA256 = Hex.toBytes(MESSAGE_SHA256_HEX);
    public static final Sha256Hash MESSAGE_SHA256_HASH = Sha256Hash.wrap(MESSAGE_SHA256);
    public static final String MESSAGE_RMD160_HEX = "04b4d07fd4b724a7343af3058910f1d153ba2d24";
    public static final byte[] MESSAGE_RMD160 = Hex.toBytes(MESSAGE_RMD160_HEX);

    /* FILE FOR HASH TESTS */

    public static File FILE;

    static {
        try {
            FILE = new File(Class.class.getResource("/SampleFile.txt").toURI());
        } catch (URISyntaxException e) {
            System.err.println("FATAL ERROR: Missing sample file!");
            System.exit(1);
        }
    }

    public static final String FILE_HEX = "5468697320697320612074657374206d657373616765";
    public static final byte[] FILE_BIN = Hex.toBytes(FILE_HEX);
    public static final String FILE_SHA256_HEX = "6f3438001129a90c5b1637928bf38bf26e39e57c6e9511005682048bedbef906";
    public static final byte[] FILE_SHA256_BIN = Hex.toBytes(FILE_SHA256_HEX);
    public static final String FILE_RMD160_HEX = "da6d79b338c3ac02a8e078334428917afb8430af";
    public static final byte[] FILE_RMD160_BIN = Hex.toBytes(FILE_RMD160_HEX);
}
