/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.crypto;

import org.junit.Test;

import java.io.IOException;

import static com.sophiatx.lwsphtxbcl.Samples.*;
import static org.junit.Assert.*;

public class Sha256Tests
{
    @Test
    public void testSha256Hash()
    {
        assertArrayEquals(MESSAGE_SHA256, Sha256Hash.hash(MESSAGE));
    }

    @Test
    public void testSha256HashTwice()
    {
        assertArrayEquals(
                Sha256Hash.hash(MESSAGE_SHA256),
                Sha256Hash.hashTwice(MESSAGE)
        );
    }

    @Test
    public void testSha256WrapBin()
    {
        assertArrayEquals(MESSAGE_SHA256, Sha256Hash.wrap(MESSAGE_SHA256).getBytes());
    }

    @Test
    public void testSha256WrapHex()
    {
        assertArrayEquals(MESSAGE_SHA256, Sha256Hash.wrap(MESSAGE_SHA256_HEX).getBytes());
    }

    @Test
    public void testSha256EqualityCheck()
    {
        Sha256Hash msgSha256 = Sha256Hash.wrap(MESSAGE_SHA256);
        assertEquals(msgSha256, Sha256Hash.wrap(MESSAGE_SHA256));
        assertNotEquals(msgSha256, Sha256Hash.wrap(FILE_SHA256_BIN));
    }

    @Test
    public void testSha256OfBin()
    {
        assertArrayEquals(FILE_SHA256_BIN, Sha256Hash.of(FILE_BIN).getBytes());
    }

    @Test
    public void testSha256OfFile()
    throws IOException
    {
        assertArrayEquals(FILE_SHA256_BIN, Sha256Hash.of(FILE).getBytes());
    }
}
