/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.crypto;

import com.sophiatx.lwsphtxbcl.Samples;
import org.junit.Test;

import java.io.IOException;

import static com.sophiatx.lwsphtxbcl.Samples.*;
import static org.junit.Assert.*;

public class Ripemd160Tests
{
    @Test
    public void testRipemd160hash()
    {
        assertArrayEquals(
                Samples.MESSAGE_RMD160,
                Ripemd160Hash.hash(Samples.MESSAGE)
        );
    }

    @Test
    public void testRipemd160WrapBin()
    {
        assertArrayEquals(MESSAGE_RMD160, Ripemd160Hash.wrap(MESSAGE_RMD160).getBytes());
    }

    @Test
    public void testRipemd160WrapHex()
    {
        assertArrayEquals(MESSAGE_RMD160, Ripemd160Hash.wrap(MESSAGE_RMD160_HEX).getBytes());
    }

    @Test
    public void testRipemd160EqualityCheck()
    {
        Ripemd160Hash msgRipemd160 = Ripemd160Hash.wrap(MESSAGE_RMD160);
        assertEquals(msgRipemd160, Ripemd160Hash.wrap(MESSAGE_RMD160));
        assertNotEquals(msgRipemd160, Ripemd160Hash.wrap(FILE_RMD160_BIN));
    }

    @Test
    public void testRipemd160OfBin()
    {
        assertArrayEquals(FILE_RMD160_BIN, Ripemd160Hash.of(FILE_BIN).getBytes());
    }

    @Test
    public void testRipemd160OfFile()
    throws IOException
    {
        assertArrayEquals(FILE_RMD160_BIN, Ripemd160Hash.of(FILE).getBytes());
    }
}
