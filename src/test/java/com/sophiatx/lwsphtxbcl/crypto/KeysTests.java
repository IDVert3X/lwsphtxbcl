/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.crypto;

import org.junit.Test;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import static com.sophiatx.lwsphtxbcl.Samples.PRIVATE_KEY;
import static com.sophiatx.lwsphtxbcl.Samples.PUBLIC_KEY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class KeysTests
{
    @Test
    public void testCreateSecp256k1KeyPair()
    throws Exception
    {
        KeyPair keyPair = Keys.createSecp256k1KeyPair();
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        assertNotNull(privateKey);
        assertNotNull(publicKey);

        assertEquals(privateKey.getEncoded().length, 144);
        assertEquals(publicKey.getEncoded().length, 88);
    }

    @Test
    public void testPublicKeyFromPrivateKey()
    {
        BigInteger computedKey = Keys.publicKeyFromPrivate(PRIVATE_KEY);
        assertEquals(computedKey, PUBLIC_KEY);
    }
}
