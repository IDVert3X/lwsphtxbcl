/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm;

import com.fasterxml.jackson.databind.JsonNode;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.DynamicGlobalProperties;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.GetDynamicGlobalPropertiesRequest;
import com.sophiatx.lwsphtxbcl.comm.exception.CommunicationException;
import com.sophiatx.lwsphtxbcl.comm.exception.ConnectionException;
import com.sophiatx.lwsphtxbcl.comm.exception.ResponseErrorException;
import com.sophiatx.lwsphtxbcl.utils.ObjectDumper;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ConnectorTests
{
    private static Connector connector;

    @BeforeClass
    public static void createConnector()
    throws UnknownHostException
    {
        connector = new Connector(
                (Inet4Address) InetAddress.getByName("socket1.sophiatx.com"),
                9193
        );
    }

    @Test
    public void testExecuteSimpleRequest()
    throws CommunicationException
    {
        AbstractRequest req = new GetDynamicGlobalPropertiesRequest();

        JsonNode res = connector.execute(req);
        System.out.println(res.toString());
    }

    @Test
    public void testExecuteMappableRequest()
    throws CommunicationException
    {
        AbstractMappableRequest<DynamicGlobalProperties> req
                = new GetDynamicGlobalPropertiesRequest();

        DynamicGlobalProperties dgp = connector.execute(req);
        ObjectDumper.print(dgp);
    }

    @Test(expected = ResponseErrorException.class)
    public void testExecuteInvalidRequest()
    throws CommunicationException
    {
        AbstractRequest req = new AbstractRequest()
        {
            @Override
            public String getMethodName()
            {
                return "non_existing_method_name";
            }

            @Override
            public Object getParams()
            {
                return new Object[0];
            }

            @Override
            protected String getApiPath()
            {
                return "whatever_api.";
            }
        };

        connector.execute(req);
    }

    @Test(expected = ConnectionException.class)
    public void testExecuteAgainstInvalidServer()
    throws UnknownHostException, CommunicationException
    {
        Connector invalidConnector = new Connector(
                (Inet4Address) InetAddress.getByName("socket1.sophiatx.com"),
                6969, // invalid port - we should not be able to connect.
                10,   // speed up the test, we are not going to receive
                10    // a response anyway, no reason to bother with waiting
        );

        invalidConnector.execute(new GetDynamicGlobalPropertiesRequest());
    }
}
