/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.comm;

import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.ApiAccountObject;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.ApiOperationObject;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.ApiVersionObject;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.ApiWitnessScheduleObject;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.DynamicGlobalProperties;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.object.OrderedObject;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.GetAccountHistoryRequest;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.GetAccountsRequest;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.GetDynamicGlobalPropertiesRequest;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.GetVersionRequest;
import com.sophiatx.lwsphtxbcl.comm.api.condenser.request.GetWitnessScheduleRequest;
import com.sophiatx.lwsphtxbcl.comm.exception.CommunicationException;
import com.sophiatx.lwsphtxbcl.utils.ObjectDumper;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;

public class CondenserApiTests
{
    private static Connector connector;

    @BeforeClass
    public static void createConnector()
    throws UnknownHostException
    {
        connector = new Connector(
                (Inet4Address) InetAddress.getByName("socket1.sophiatx.com"),
                9193
        );
    }

    @Test
    public void testGetAccountHistory()
    throws CommunicationException
    {
        Collection<OrderedObject<ApiOperationObject>> history;
        history = connector.execute(new GetAccountHistoryRequest("abc", -1, 5));
        for (OrderedObject<ApiOperationObject> o : history) {
            ObjectDumper.print(o.getObject());
        }
    }

    @Test
    public void testGetAccounts()
    throws CommunicationException
    {
        Collection<ApiAccountObject> accounts;
        accounts = connector.execute(new GetAccountsRequest("kucoin"));
        for (ApiAccountObject acc : accounts)
            ObjectDumper.print(acc);
    }

    @Test
    public void testGetDynamicGlobalProperties()
    throws CommunicationException
    {
        DynamicGlobalProperties dgp = connector.execute(new GetDynamicGlobalPropertiesRequest());
        ObjectDumper.print(dgp);
    }

    @Test
    public void testGetVersion()
    throws CommunicationException
    {
        ApiVersionObject v = connector.execute(new GetVersionRequest());
        ObjectDumper.print(v);
    }

    @Test
    public void testGetWitnessSchedule()
    throws CommunicationException
    {
        ApiWitnessScheduleObject vs = connector.execute(new GetWitnessScheduleRequest());
        ObjectDumper.print(vs);
    }
}
