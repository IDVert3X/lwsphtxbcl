/*
 * Copyright 2018-2019 SophiaTX foundation, and contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sophiatx.lwsphtxbcl.utils.conversion;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class HexStringConverterTests
{
    private static DummyObject obj;
    private static String str = "{\"v\":\"204f\"}";
    private static ObjectMapper mapper;

    @BeforeClass
    public static void prepare()
    {
        obj = new DummyObject(new Byte[]{32, 79});
        mapper = new ObjectMapper();
    }

    @Test
    public void testSerialization()
    throws JsonProcessingException
    {
        assertEquals(str, mapper.writeValueAsString(obj));
    }

    @Test
    public void testDeserialization()
    throws IOException
    {
        assertEquals(obj, mapper.readValue(str, DummyObject.class));
    }

    private static class DummyObject
    {
        @JsonSerialize(using = HexStringSerializer.class)
        @JsonDeserialize(using = HexStringDeserializer.class)
        @JsonProperty("v")
        public Byte[] value;

        @JsonCreator
        public DummyObject(@JsonProperty("v") Byte[] value)
        {
            this.value = value;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DummyObject that = (DummyObject) o;
            return Arrays.equals(value, that.value);
        }

        @Override
        public int hashCode()
        {
            return Arrays.hashCode(value);
        }
    }
}
