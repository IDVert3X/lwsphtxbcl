# Lightweight SophiaTX Blockchain Library

This lightweight library allows you to interact with the SophiaTX blockchain.

It's still under active development, and there is absolutely no guarantee that 
the APIs will stay the same as they are now. Not all the operations and daemon
methods are implemented yet, neither is everything documented.

There are usually multiple ways to approach the same problem.  
A lower level ones give you more control and allow you to optimize the code,
although they do require a bit of understanding of the blockchain concepts.

A higher level ones, on the other hand, give you very little to no control, but
can often get the job done in just one line of code. This is great for quick
start, rapid prototyping and applications that don't need to scale and deal
with many transactions being sent ( in which case, you may want to avoid using
the highest level wrappers and write a more optimized code instead ).

Also note that all the objects are annotated with Jackson annotations. There is 
no need to write serializers / deserializers on your side, neither is there a
need for special ObjectMapper configuration.

## Keys   

### Private key
Represented as a `PrivateKey` object in this library.  
Private key is a 32 byte ( 256 bit ) integer.    
Textual representation of private key is `WIF` ( Wallet Import File ).  
WIF consists of: `base58(0x80 + privKey[32B] + checksum[4B])`    
```java
PrivateKey privKey;
privKey = new PrivateKey("5KC66cEPcxbpHY5zNaBg8G3ZVhVSNAcqefV9trZuThyrHkAWGfx");
privKey = new PrivateKey(new BigInteger("<very long number>"))

privKey.getKey()   // <very long number>
privKey.toString() // 5KC66cEPcxbpHY5zNaBg8G3ZVhVSNAcqefV9trZuThyrHkAWGfx

// to obtain a public key from the private key, use this:
PublicKey pubKey = privKey.derivePublicKey(); 
```

### Public key
Represented as a `PublicKey` object in this library.  
Public key consists of two 32 byte integers compressed into one 33 byte integer.  
Textual representation of a public key is very similar to WIF key.  
Public key string consists of: `"SPH" + base58(pubKey[33B] + checksum[4B])`

```java
PublicKey pubKey;
pubKey = new PublicKey("SPH6D1rTi6g2WB8V2inFQ8yYa9dxoZ8xU1VN3w28tT1PAQEdwi1hd");
pubKey = new PublicKey(new BigInteger("<very long number>"))

pubKey.getKey()   // <very long number>
pubKey.toString() // SPH6D1rTi6g2WB8V2inFQ8yYa9dxoZ8xU1VN3w28tT1PAQEdwi1hd
```

### Encoding and decoding keys in bulk
There is a `KeyEncoder` class which is used to convert textual representation of
the keys to BigInteger and back. `PrivateKey` and `PublicKey` objects use this
class to do the conversion. You can use this class for processing of keys in 
bulk, as this method is slightly faster than creating an object for each 
individual key and then calling methods on it just to convert it.
```java
BigInteger key = KeyEncoder.decodeWif("<WIF key>")
String     wif = KeyEncoder.encodeWif(key);
```
```java
BigInteger key = KeyEncoder.decodePublicKey("<PubKey>");
String     pub = KeyEncoder.encodePublicKey(key);
```

### Key Pair
Represented as a `KeyPair` object in this library.  
This object is mainly used for cryptography, e.g. signing transactions.

Import a key pair:
```java
// available constructors:
// KeyPair(BigInteger, BigInteger)
// KeyPair(PrivateKey, PublicKey)
// KeyPair(String (WIF), String (public key))
KeyPair kp = new KeyPair(privKey, pubKey);
```

If you don't have a public key, you may omit it, it will be derived from the
private key.
```java
// available constructors: 
// KeyPair(BigInteger)
// KeyPair(PrivateKey)
// KeyPair(String (WIF))
KeyPair kp = new KeyPair(privKey)
```

If you need a new key pair, simply generate it like this:
```java
KeyPair kp = KeyPair.generate();
```

## Networking
In order to talk to the SophiaTX blockchain, we need to be able to connect to it
somehow. To do that, this library includes so called `Connector`. This class is
responsible for handling requests and responses. You only need one instance of
this object unless you are talking to a multiple nodes.
```java
Inet4Address host = (Inet4Address) InetAddress.getByName("socket1.sophiatx.com");
Connector socket1 = new Connector(host, 9193);
```

## SophiaTX helper class
Some of the most common functionality is wrapped by the `SophiaTX` helper class.
If you prefer a high level coding option, you may find `SophiaTX` class very
useful, it's especially great for rapid prototyping, at a cost of fine control
over your code and potential performance impact if not used properly. As some of
the functionality included in this helper class requires a connection to the
blockchain ( e.g. to pull a current head block and create a transaction from it 
), you need to initialize it with the connector you instantiated in the previous 
step. You may need to instantiate this class for each connector you have.

Please note that instantiating this class will execute "get_version" method on
the node in order to obtain a chain ID - this is needed to create signatures 
that are valid for the network specified in the Connector instance only.
```java
SophiaTX sophiaTX = new SophiaTX(connectorInstance);
```

### Executing the most common operations

Unless you are sending lots of transactions where performance and data transfers
matter, you can just execute the most common operations in a single line of code
( excluding error handling ) using this wrapper. Be careful that this can have a 
huge performance impact if used to send lots of transactions at once.

For every call, the helper class will connect to the blockchain node in the,
pull current head block, conversion rate and broadcast the transaction. That's 3 
API calls - not that much if you want to send just one transaction, but scales 
terribly when you need to send hundreds or even just 10 of them at once.

Available operations:
```java
// all these methods return transaction ID as a String and throw CommunicationException

sophiaTX.transfer(String from, String to, BigDecimal amount, String memo, KeyPair kp);
sophiaTX.transferToVesting(String from, String to, BigDecimal amount, KeyPair kp);
sophiaTX.withdrawVesting(String account, BigDecimal amount, KeyPair kp);
sophiaTX.vote(String votingAccount, String witness, boolean approve, KeyPair kp);
sophiaTX.sponsorFees(String sponsor, String sponsored, boolean isSponsoring, KeyPair kp);
sophiaTX.sendJson(String sender, List<String> recipients, long appId, String json, KeyPair kp);

// more calls will be added in the future...
``` 
#### 

## Unsigned numbers

This library includes `UByte`, `UShort` and `UInteger` classes. These represent
8, 16 and 32 bit unsigned integers respectively. They are needed because Java 
represents all the numbers as signed integer types, while the blockchain often 
uses unsigned ones. This classes take care of this and provide some additional 
type safety.

## Blocks

Each block has a `number` ( height ), which is a 32 bit unsigned integer. Short 
representation of this number exists and is often referred to as a `num`. It 
consists of a 16 least significant bits of the block number. It's used for fast 
referencing of the last 65536 blocks. Transactions refer to this num instead of 
the number. That means only the last 64k blocks are referenceable.

Another important attribute of the block is a `block_id` ( 160 bits long ).  
This consists of a block's RIPEMD-160 hash, part of which was replaced by a 
block number.  
This library represents it as a `BlockId` object, usage goes like that:
```java
BlockId blockId = new BlockId("000031fd5ca3c50ce68d0e3c538500b360fb6da5");

String   id     = blockId.toString();       // block id, same as above
UInteger number = blockId.getBlockNumber(); // block number ( height )
UShort   num    = blockId.getBlockNum();    // used for transactions
UInteger prefix = blockId.getBlockPrefix(); // used for transactions
```

There are some additional attributes:  
`previous` - previous block ID  
`timestamp` - when the block was created  
`witness` - name of the witness that created the block  
`transaction_merkle_root` - TODO: figure out what the hell is it  
`extensions` - array of extensions, not used & not implemented, safe to ignore  
`witness_signature` - 130 bits long signature  
`signing_key` - public key derived from a private key that was used for signing

## Signatures
Represented as a `CompactSignature` object in this library.

Signatures consist of two coordinates, `r` and `s`, both of which are 32 bytes
long integers. There is also a `recId` parameter which is a tiny integer used to
recover a public key from the signature..
  
TODO: compact signature docs

## Transactions

This library includes a simple to use transaction builder.

There are two classes: `Transaction` and `SignedTransaction`.  
While they may sound self-explanatory, it's important to note that a 
`Transaction` object can not be broadcast, you need to sign a `Transaction`
object first, which will return a `SignedTransaction` object that can be
broadcast once you are done adding signatures.  
It's also important to keep in mind that `SignedTransaction` object is
immutable, you may not change its content, as that would render the previously
added signatures invalid. 

Transaction in SophiaTX consist of a block reference, expiration, operations and
signatures.  

Each transaction references a certain block in the past ( preferably the latest
one available during the creation of a transaction ). This is done using
`ref_block_num` and `ref_block_prefix` properties.

### Instantiating the transaction

A _low-level_ approach consists of providing the reference block data manually.  
Such an approach does not really make much of a sense, but it's there in case 
you need it, whatever the reason might be.
```java
// create a Transaction from ref_block_num and ref_block_prefix
Transaction tx = new Transaction(UShort.valueOf(32441), UInteger.valueOf(2784635841L))
```

A _high level_ approach is much simpler, just use the `SophiaTX` wrapper:
```java
// connect to the network, obtain ref. block data and create a new Transaction
Transaction tx = sophiaTX.createTransaction();
```

**Notice**: while this approach is simple and clean, it's no good when you need 
to instantiate/create many transactions, as it connects to the network to obtain
the reference data every single time `createTransaction()` method is called. 

If you need to create multiple transactions, obtain the head block ID and re-use 
it like this instead:
```java
// connect to the network once to obtain the head block
BlockId refBlock = sophiaTX.getHeadBlockId();
// initialize all the transaction using this head block
Transaction tx01 = new Transaction(refBlock);
Transaction tx02 = new Transaction(refBlock);
```

### Adding operations

Now when we have our transaction object ready, let's add some operations.  
Each operation is an action that will be executed on the blockchain.  
Notice that for every operation, we have to specify the currency in which we
want to pay for the fee.
```java
myTx.addOperation(new AccountWitnessVoteOperation(
        "linus", // vote as "linus"
        "nick",  // vote for "nick"
        false    // vote "no"
    ))
    .addOperation(new TransferOperation(
        "linus",                              // send from "linus"
        "luke",                               // send to "luke"
        new Asset("1.95", AssetSymbol.SPHTX), // transfer 1.95 SPHTX
        "This is a TX builder demo"           // memo
    ));
```
Notice that `Asset` object is used to represent assets ( currencies ).

### Calculating fees

Another important step is to calculate fees for the operations. Fees are fixed
to USD and in order to calculate those fees ( in SPHTX ), we need to know the 
current conversion ( exchange ) rate. This can easily be obtained using SophiaTX
helper class.

**IMPORTANT: this needs to be done before signing the transaction!**  
That's because the fee is a part of the signature itself.

```java
// connect to the network once to obtain the conversion rate
ConversionRate sphtxToUSD = sophiaTX.getConversionRateUSD();

// calculate the fees for the transaction(s)
myTx.calculateFees(sphtxToUSD);
myOtherTx.calculateFees(sphtxToUSD);
// ...
```

### Defining expiration

One more thing you should care about is the expiration.  
Expiration is a timestamp after which your TX will be dropped if it was not 
processed.  
Unless you are fine with default expiration value of 30 seconds after the object 
has ben created, you can change it either by using relative or absolute value:
```java
myTx.expireIn(10); // expire in 10 seconds since calling this method
// or...
myTx.expireAt(LocalDateTime.parse("2018-12-24T12:00:00"));
```
That's it, you have just successfully assembled your first transaction.

### Signing

All you need to do is to call the `sign(KeyPair)` method on the `Transaction`
object. This method will return a `SignedTransaction` object that extends it.
Remember that `SignedTransaction` is immutable, as any change in this object 
would render the existing signatures invalid. You can keep adding more 
signatures as needed, sign method works as a builder ( returns `this` ) here.
```java
SignedTransaction mySignedTx = myTx.sign(keyPair);

// you can keep adding additional signatures as needed
mySignedTx.sign(keyPair2)
          .sign(keyPair3)
          .sign(keyPair4);
```

### Broadcasting

Want to broadcast it? Doesn't get much simpler than that:
```java
mySignedTx.broadcast(connector)
```

To send multiple transactions at once ( in a single request ), you can use the
in-built API layer this library provides, `BroadcastTransactionRequest` to be
more specific.
```java
// for a single transaction
connector.execute(new BroadcastTransactionRequest(mySignedTx));

// if you have multiple transactions to broadcast
BroadcastTransactionRequest req = new BroadcastTransactionRequest()
    .addTransaction(tx1)
    .addTransaction(tx2)
    .addTransaction(txN);
connector.execute(req);

// or if you already have a collection of transactions:
connector.execute(new BroadcastTransactionRequest(txCollection));
```

### Full example: assembling a transaction to send tokens
```java
// initialize a new transaction
Transaction myTx;
try {
    myTx = sophiaTX.createTransaction();
} catch (CommunicationException e) {
    // failed to obtain reference block
}

// obtain conversion rate
ConversionRate conversionRate;
try {
    conversionRate = sophiaTX.getConversionRateUSD();
} catch(CommunicationException e) {
    // failed to obtain exchange rate
}

// assemble the transaction
myTx.addOperation(new TransferOperation(
        "linus",                              // send from "linus"
        "luke",                               // send to "luke"
        new Asset("1.95", AssetSymbol.SPHTX), // send 1.95 SPHTX
        "This is a TX builder demo"           // memo
    ))
    .calculateFees(conversionRate)
    .expireAt(LocalDateTime.parse("2018-12-24T12:00:00"));

// sign the transaction
SignedTransaction mySignedTx = myTx.sign(keyPair);

// broadcast the signed transaction
try {    
    mySignedTx.broadcast(connector);
} catch (ResponseErrorException e) {
    // something is wrong with the transaction
} catch (CommunicationException e) {
    // failed to broadcast the transaction
}
    
System.out.println("Successfully broadcast, view at: " +
                   "https://explorer.sophiatx.com/transactions/"+ mySignedTx.getId());
```
Or if you don't care about the individual errors:
```java
try {
    sophiaTX.createTransaction()
            .addOperation(new TransferOperation(
                "linus",                              // send from "linus"
                "luke",                               // send to "luke"
                new Asset("1.95", AssetSymbol.SPHTX), // send 1.95 SPHTX
                "This is a TX builder demo"           // memo
            ))
            .calculateFees(sophiaTX.getConversionRateUSD())
            .expireAt(LocalDateTime.parse("2018-12-24T12:00:00"))
            .sign(keyPair)
            .broadcast(connector);
} catch (CommunicationException e) {
    // something went wrong
}
```

## Connector and requests

TODO

## Hash classes

TODO

## Util classes

TODO

## Error handling

TODO

## Low level crypto stuff

TODO

## Asset, Authority, ...

TODO